#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set sw=4 ts=4 fdm=indent foldnestmax=3 ft=python et:

import re, pdb, types, functools, itertools, math
from array import array
from copy import copy, deepcopy

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
import LifetimeFitter.cpp
from v2Fitter.Fitter.DataReader import DataReader
from v2Fitter.Fitter.ObjProvider import ObjProvider
from LifetimeFitter.varCollection import dataArgs, Bmass, MCArgSet, treco
from LifetimeFitter.anaSetup import tbins, bMassRegions, modulePath 
from LifetimeFitter.python.datainput import genSel, ExtraCuts

from ROOT import TChain, TEfficiency, TH2D, RooArgList, RooDataHist

CFG = DataReader.templateConfig()
CFG.update({
    'argset': dataArgs,
    'lumi': -1,  # Keep a record, useful for mixing simulations samples
    'weight': '1',
    #'ifriendIndex': ["Bmass", "Mumumass"],
})

# dataReader
def customizeOne(self, targetBMassRegion=None, extraCuts=None):
    """Define datasets with arguments."""
    if targetBMassRegion is None:
        targetBMassRegion = []
    if not self.process.cfg['binKey'] in tbins.keys():
        self.logger.logERROR("Bin {0} is not defined.\n".format(self.process.cfg['binKey']))
        raise ValueError

    # With shallow copied CFG, have to bind cfg['dataset'] to a new object.
    self.cfg['dataset'] = []
    for key, val in bMassRegions.items():
        finalcut = self.process.cfg['cuts'][-1]
        if any([re.match(pat, key) for pat in targetBMassRegion]):
            self.cfg['dataset'].append(
                (
                    "{0}.{1}".format(self.cfg['name'], key),
                    "({0}) && ({1}) && ({2}) && ({3})".format(
                        val['cutString'],
                        tbins[self.process.cfg['binKey']]['cutString'],
                        finalcut, #self.process.cfg['cuts'][-1],
                        "1" if not extraCuts else extraCuts,
                    )
                )
            )
    # Customize preload TFile
    self.cfg['preloadFile'] = modulePath + "/data/preload_{datasetName}_{Year}_{binLabel}.root".format(
                            datasetName=self.cfg['name'].split('.')[0], 
                            Year=self.process.cfg['args'].Year,  
                            binLabel=tbins[self.process.cfg['binKey']]['label'])

# sigMCGENReader
def customizeGEN(self):
    """Define datasets with arguments."""
    if not self.process.cfg['binKey'] in tbins.keys():
        print("ERROR\t: Bin {0} is not defined.\n".format(self.process.cfg['binKey']))
        raise AttributeError

    # With shallow copied CFG, have to bind cfg['dataset'] to a new object.
    self.cfg['dataset'] = []
    self.cfg['dataset'].append(
        (
            "{0}.Fit".format(self.cfg['name']),
            re.sub("Q2", "genQ2", tbins[self.process.cfg['binKey']]['cutString']) + " && genKpPt>0 && genKmPt>0"
        )
    )
    # Customize preload TFile
    self.cfg['preloadFile'] = modulePath + "/data/preload_{datasetName}_{Year}_{binLabel}.root".format(datasetName=self.cfg['name'].split('.')[0], Year=self.process.cfg['args'].Year, binLabel=tbins[self.process.cfg['binKey']]['label'])

def GetDataReader(self, seq):
    Year=self.cfg['args'].Year
    if seq == 'dataReader':
        dataReaderCfg = copy(CFG)
        dataReaderCfg.update({
            'name': "dataReader.{Year}".format(Year=Year),
            'ifile': self.cfg['dataFilePath'],
            'lumi': 35.92 if Year==2016 else 41.53 if Year==2017 else 59.74, })
        dataReader = DataReader(dataReaderCfg)
        customizeData = functools.partial(customizeOne, targetBMassRegion=['^Fit$', '^.{0,1}SB$'], extraCuts=ExtraCuts) #'^Fit$', '^.{0,1}SB$',
        dataReader.customize = types.MethodType(customizeData, dataReader)
        return dataReader

    # sigMCReader
    if seq == 'sigMCReader':
        sigMCReaderCfg = copy(CFG)
        sigMCReaderCfg.update({
            'name': "sigMCReader.{}".format(Year),
            'ifile': self.cfg['sigMC'],
            'argset': MCArgSet,
            'weight': 'Puw8',
            'lumi': 66362.21 if Year==2016 else 36139.99 if Year==2017 else 26998.25,
        })
        sigMCReader = DataReader(sigMCReaderCfg)
        customizeSigMC = functools.partial(customizeOne, targetBMassRegion=['^Fit$'], extraCuts="is_truebs == 1")
        sigMCReader.customize = types.MethodType(customizeSigMC, sigMCReader)
        return sigMCReader
    if seq == 'peakMCReader':
        peakMCReaderCfg = copy(CFG)
        peakMCReaderCfg.update({
            'name'  : "peakMCReader.{}".format(Year),
            'ifile' : self.cfg['peaking']['BdMC'],
            'argset': MCArgSet,
            'weight': 'Puw8',
            'lumi'  : 6636,
        })
        peakMCReader = DataReader(peakMCReaderCfg)
        customizePeakMC = functools.partial(customizeOne, targetBMassRegion=['^Fit$'], extraCuts="is_truebs == 1")
        peakMCReader.customize = types.MethodType(customizePeakMC, peakMCReader)
        return peakMCReader
    if seq == 'StatusTableMaker':
        import LifetimeFitter.fitCollection  as fitCollection
        from LifetimeFitter.script.latex.LatexTables import StdTableMaker
        from LifetimeFitter.seqCollection import Instantiate
        try:
            names={}
            for Year in [2016, 2017, 2018]:
                self.cfg['args'].Year = Year
                names[str(Year)] = [Instantiate(self, [seq])[0].name for seq in self.cfg['args'].TSeq]
        except:
            print("IndexError: One of the sequences from {} not found".format(self.cfg['args'].TSeq))

        setupLatexTables = copy(StdTableMaker.templateConfig())
        setupLatexTables.update({
            "Titles" : ["Migrad", "Hesse", "Minos", "covQual", "FCN"],
            "DbValue": ["MIGRAD", "HESSE", "MINOS", "covQual", "nll"]
        })
        LatexTableMaker = StdTableMaker(setupLatexTables)

        for Year in ['2016', '2017', '2018']:
            LatexTableMaker.ccfg[Year] = {}
            for TSeq in self.cfg['args'].TSeq:
                LatexTableMaker.ccfg[Year][TSeq] = copy(setupLatexTables)
        for Year in ['2016', '2017', '2018']:
            for TSeq, name in zip(self.cfg['args'].TSeq, names[Year]):
                LatexTableMaker.ccfg[Year][TSeq]['DbNames'] = ["{}.StdFitter".format(name) for a in LatexTableMaker.ccfg[Year][TSeq]['Titles']]
                if TSeq == 'effiFitter':
                    LatexTableMaker.ccfg[Year][TSeq]['DbNames'] = ["{}.CosThetaK".format(name) for a in LatexTableMaker.ccfg[Year][TSeq]['Titles']] +\
                                                  ["{}.CosThetaL".format(name) for a in LatexTableMaker.ccfg[Year][TSeq]['Titles']] + \
                                                  ["{}.XTerm".format(name) for a in LatexTableMaker.ccfg[Year][TSeq]['Titles']]
                    LatexTableMaker.ccfg[Year][TSeq]['Titles']  = LatexTableMaker.ccfg[Year][TSeq]['Titles']*3
                    LatexTableMaker.ccfg[Year][TSeq]['DbValue'] = ["MINIMIZE", "HESSE", "MINOS", "covQual", "nll"]*3
        return LatexTableMaker

setupEfficiencyBuildProcedure = {}
setupEfficiencyBuildProcedure['sig'] = {
    'ifiles': None,
    'baseString': None,
    'cutString' : None,
    'fillXY': "genCosThetaK:genCosThetaL",  # Y:X
    'weight': None
}
setupEfficiencyBuildProcedure['peak'] = {
    'ifiles': None,
    'dfiles': None,
    'baseString': None,
    'cutString': None,
    'fillXY': "CosThetaK:CosThetaL",  # Y:X
    'fillXYDen': "genCosThetaK:genCosThetaL",  # Y:X
    'weight': None
}

# effiHistReader
sigTBins = array('d', [0., 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.6, 0.7, 0.8, 0.9, 1., 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 2.6, 2.8, 3., 3.2, 3.4, 3.6, 3.8, 4.0, 4.4, 4.8, 5.2, 5.6, 6, 6.4, 8, 10])
peakTBins = array('d', [0., 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.6, 0.7, 0.8, 0.9, 1., 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 2.6, 2.8, 3., 3.2, 3.4, 3.6, 3.8, 4.0, 4.4, 4.8, 5.2, 5.6, 6, 6.4, 8, 10])

def _buildEfficiency(self, binKey, Year, sigArgs, peakArgs):
    for channel, sample, Bins, genLifetime in [sigArgs, peakArgs]:
        setupEfficiencyBuildProcedure[channel].update({
            'ifiles'    : sample,
            'baseString': tbins[binKey]['cutString'],
            'cutString' : "({0}) && ({1}) && ({2})".format(self.process.cfg['cuts'][-1], tbins[binKey]['cutString'], 1 if ExtraCuts==None else ExtraCuts),
            'fillXY'    : "treco"
        })

        treein = TChain()
        for f in setupEfficiencyBuildProcedure[channel]['ifiles']: treein.Add(f)

        # Save reco level lifetime distribution to h_passsed 
        treein.Draw(">>passedEvtList", setupEfficiencyBuildProcedure[channel]['cutString'])
        passedEvtList = ROOT.gDirectory.Get("passedEvtList")
        h_passed = ROOT.TH1D("h_passed_{}_{}".format(binKey, channel), "", len(Bins)-1, Bins)
        treein.SetEventList(passedEvtList) 
        treein.Draw("{0}>>{1}".format(setupEfficiencyBuildProcedure[channel]['fillXY'], h_passed.GetName()), "", "goff")

        # Generate PDG lifetime distribution to be used as denominator of efficiency
        tau      = ROOT.RooRealVar("tau", "tau", genLifetime)
        genModel = ROOT.RooGenericPdf("name", "title", "exp(-treco/@0)", ROOT.RooArgSet(tau, treco))
        data     = genModel.generate(ROOT.RooArgSet(treco), 2000000)
        binning  = ROOT.RooBinning(len(Bins)-1, Bins)
        h_total  = data.createHistogram("h_total_{}_{}".format(binKey, channel), treco, ROOT.RooFit.Binning(binning))

        h_eff = ROOT.TEfficiency(h_passed, h_total)
        h_eff.Write("TEff_{}_{}_{}".format(channel, Year, binKey), ROOT.TObject.kOverwrite)

        # Convert TEfficiency to TH1
        h_hist = h_eff.GetCopyPassedHisto()
        h_hist.Reset("ICESM")
        for b in range(1, len(Bins)):
            h_hist.SetBinContent(b, h_eff.GetEfficiency(b))
            h_hist.SetBinError(b, max(h_eff.GetEfficiencyErrorLow(b), h_eff.GetEfficiencyErrorUp(b)))
        h_hist.Write("Hist_{}_{}_{}".format(channel, Year, binKey), ROOT.TObject.kOverwrite)

def buildTotalEffiHist(self):
    """Build efficiency histograms for later fitting/plotting"""
    binKey = self.process.cfg['binKey']
    Year   = self.process.cfg['args'].Year
    fin = ROOT.TFile(modulePath + "/data/EfficiencyHists_{0}_{1}.root".format(Year, tbins[binKey]['label']), "UPDATE")

    sigArgs  = ('sig', self.process.cfg['sigMC'], sigTBins, 1.472)
    peakArgs = ('peak', self.process.cfg['peaking']['BdMC'], peakTBins, 1.525)
    sigHist  = fin.Get("Hist_sig_{}_{}".format(Year, binKey))
    if sigHist==None:
        print(">> Buiding efficiency histograms")
        _buildEfficiency(self, binKey, Year, sigArgs, peakArgs)

    # Register the chosen one to sourcemanager
    sigHist = fin.Get("Hist_sig_{}_{}".format(Year, binKey))
    self.cfg['source']['Eff_sigHist_{0}'.format(Year)] = copy(sigHist)
    self.cfg['source']['Eff_sig_{0}'.format(Year)] = RooDataHist("effi_sig", "", RooArgList(treco), ROOT.RooFit.Import(sigHist))
    peakHist = fin.Get("Hist_peak_{}_{}".format(Year, binKey))
    self.cfg['source']['Eff_peakHist_{0}'.format(Year)] = copy(peakHist)
    self.cfg['source']['Eff_peak_{0}'.format(Year)] = RooDataHist("effi_peak", "", RooArgList(treco), ROOT.RooFit.Import(peakHist))

effiHistReader = ObjProvider({
    'name': "effiHistReader",
    'obj': {'effiHistReader': [buildTotalEffiHist,],}
})

import LifetimeFitter.script.latex.makeTables as makeTables
FinalDataResult = ObjProvider({
    'name': "FinalDataResult",
    'obj': {'FinalDataResult': [makeTables.table_dataresAFBFL, ], }
})
EffiTable = ObjProvider({'name': "EffiTable",  'obj': {'EffiTable': [makeTables.EffiTable] }})
PeakFracTable = ObjProvider({'name': "PeakFracTable",  'obj': {'PeakFracTable': [makeTables.PeakFracTable] }})

import LifetimeFitter.python.CompVar as CompVar
GetCompPlots = ObjProvider({
    'name': "GetCompPlots",
    'obj': {'GetCompPlots': [CompVar.GetCompPlots, ], }
})

if __name__ == '__main__':
    pass
