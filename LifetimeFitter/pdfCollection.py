#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set sw=4 ts=4 fdm=indent fdn=3 ft=python et:

# Description     : Define PDFs
# Author          : Pritam Kalbhor (physics.pritam@gmail.com)
#                   Last Modified   : 02 Aug 2020 12:04 01:26

############
# WARNINGS #
############
# Dont call TObject.Print(), it seems the iterators leads to random crash
# In RooWorkspace.factory(), you MUST replace the calculation between numbers to a single float number, e.g. 2/3 -> 0.666667
# It is possible that the parser don't designed to handle RooAddition and RooProduct between RooConstVar

import types, sys, pdb, ROOT
import functools
from copy import copy, deepcopy
from collections import OrderedDict

from v2Fitter.Fitter.ObjProvider import ObjProvider
from v2Fitter.Fitter.WspaceReader import WspaceReader

from LifetimeFitter.StdProcess import isDEBUG
from LifetimeFitter.anaSetup import modulePath, tbins
from LifetimeFitter.varCollection import Bmass, treco, trecoe
import LifetimeFitter.cpp
ROOT.PyConfig.IgnoreCommandLineOptions = True
from ROOT import RooWorkspace, RooEffProd, RooKeysPdf

def getWspace(self):
    """Read workspace"""
    wspaceName = "wspace.{0}.{1}".format(self.process.cfg['args'].Year, self.cfg.get("wspaceTag", "DEFAULT"))
    if wspaceName in self.process.sourcemanager.keys():
        wspace = self.process.sourcemanager.get(wspaceName)
    else:
        if not isDEBUG:
            self.logger.logERROR("RooWorkspace '{0}' not found".format(wspaceName))
            self.logger.logDEBUG("Please access RooWorkspace with WspaceReader")
            raise RuntimeError
        wspace = RooWorkspace(wspaceName)
        self.process.sourcemanager.update(wspaceName, wspace)
    wspace.addClassDeclImportDir(modulePath+ '/../')
    wspace.addClassImplImportDir(modulePath) #+ '/cpp')
    wspace.importClassCode(ROOT.MyDecay.Class())
    return wspace

setattr(ObjProvider, 'getWspace', getWspace) 

#########################
# Now start define PDFs #
#########################


def buildGenericObj(self, objName, factoryCmd, varNames, CopyObj=None):
    """Build with RooWorkspace.factory. See also RooFactoryWSTool.factory"""
    wspace = self.getWspace()
    tspace = None
    obj = wspace.obj(objName)
    def InFactory(wspace):
        for v in varNames:
            if wspace.obj(v) == None:
                getattr(wspace, 'import')(globals()[v])  #Import CosThetaK and CosThetaL 
        for cmdIdx, cmd in enumerate(factoryCmd):
            wspace.factory(cmd)
    if obj == None:
        self.logger.logINFO("Build {0} from scratch.".format(objName))
        InFactory(wspace)
        obj = wspace.obj(objName)
    elif objName=='effi_sigA':
        tspace = RooWorkspace('TempWorkSpace')
        InFactory(tspace)

    if CopyObj is not None:
        for suffix, Obj in CopyObj:
            if not tspace==None:
                getattr(wspace, 'import')(tspace.obj(Obj), ROOT.RooFit.RenameAllNodes(suffix), ROOT.RooFit.RenameAllVariablesExcept(suffix, 'Bmass,CosThetaL,CosThetaK'))
            else:
                getattr(wspace, 'import')(wspace.obj(Obj), ROOT.RooFit.RenameAllNodes(suffix), ROOT.RooFit.RenameAllVariablesExcept(suffix, 'Bmass,CosThetaL,CosThetaK'))
            self.cfg['source'][Obj+'_'+suffix]=wspace.obj(Obj+'_'+suffix)
    del tspace
    self.cfg['source'][objName] = obj

#---------------------------------------------------------------------------------------------------
# Efficiency PDFs
def initParameters(self):
    Year = self.process.cfg['args'].Year
    params = {'k0': "k0[4.36627e-04, 0,1.]",     'k1': "k1[-.66842e+00, -30,30]", 
              'k2': "k2[-7.97688e-02, -40, 40]", 'k3': "k3[2.29e+01, -600, 600]", 
              'k4': "k4[2.7728e+00, -100,100]",
              'l0': "l0[4.36627e-04, 0,1.]",     'l1': "l1[-.66842e+00, -30,30]", 
              'l2': "l2[-7.97688e-02, -40, 40]", 'l3': "l3[2.29e+01, -600, 600]", 
              'l4': "l4[2.7728e+00, -100,100]",
              }
    if Year == 2016: 
        params.update({
        'k1': "k1[-5.66842e+00, -30,30]", 'k3': "k3[7.29125e+01, -600, 600]",
        'k4': "k4[2.07728e+00, -100,100]",
        'l1': "l1[-5.66842e+00, -30,30]", 'l3': "l3[7.29125e+01, -600, 600]",
        'l4': "l4[2.07728e+00, -100,100]",
        })
        return params
    if Year == 2017: return params
    if Year == 2018: return params

f_effiSig_format = {}
f_effiSig_format['default'] = ["expr::effi_sig(' TMath::Max(1e-7, k0*(1+k1*treco+k2*pow(treco,2)+k3/(1+exp(-treco*k4))) ) ', {treco, k0, k1, k2, k3, k4})"] + \
                              ["expr::effi_peak(' TMath::Max(1e-7, l0*(1+l1*treco+l2*pow(treco,2)+l3/(1+exp(-treco*l4))) ) ', {treco, l0, l1, l2, l3, l4})"]

def Build_EfficiencyPDFs(self):
    variables = initParameters(self)
    f_effiSig_format['full'] =  [variables[i] for i in variables]+f_effiSig_format['default']
    return f_effiSig_format.get(self.process.cfg['binKey'], f_effiSig_format['default'])

setupBuild_EfficiencyPDFs = {
    'objName': "effi_sig",
    'varNames': ["treco"],
    'factoryCmd' : []
}


#---------------------------------------------------------------------------------------------------
setupBuild_MassPDFs = {
    'objName': "MassPDF",
    'varNames': ["Bmass"],
    'factoryCmd' : [
        # "RooJohnson::f_sigM1(Bmass, sigM_mu1[5.31, 5.25, 5.45], sigM1_lambda[0.96, 1e-6, 1], sigM1_gamma[-6.03, -10, 10], sigM1_delta[5.30, 1e-7, 10])",
        # "RooJohnson::f_sigM2(Bmass, sigM_mu2[5.32, 5.25, 5.45], sigM2_lambda[0.36, 1e-6, 1], sigM2_gamma[-0.75, -10, 10], sigM2_delta[9.27, 1e-6, 10])",
        # "SUM::f_sigM(sigM_frac[0.00517, 0.,1.]*f_sigM1, f_sigM2)",

        "RooGaussian::f_sigM1(Bmass, sigM_mu[5.37, 5.3, 5.4], sigM_sigma1[0.01, 0.001, 1])",
        "RooGaussian::f_sigM2(Bmass, sigM_mu, sigM_sigma2[0.03, 0.001, 1])",
        "RooGaussian::f_sigM3(Bmass, sigM_mu, sigM_sigma3[0.04, 0.001, 1])",
        "SUM::f_sigM(sigM_frac1[0.01, 0.,1.]*f_sigM1, sigM_frac2[0.1, 0.,1.]*f_sigM2, f_sigM3)",

        # "RooJohnson::f_bkgPeakM1(Bmass, bkgPeakM_mu[5.279, 5.2, 5.35], bkgPeakM1_lambda1[0.017, 1e-6, 1], bkgPeakM1_gamma[-0.0245, -1, 1], bkgPeakM1_delta[0.8647, 1e-6, 5])",
        # "RooJohnson::f_bkgPeakM2(Bmass, bkgPeakM_mu, bkgPeakM2_lambda[0.0184, 1e-6, 1], bkgPeakM2_gamma[-0.0218, -10, 10], bkgPeakM2_delta[1.456, 1e-6, 5])",
        # "SUM::f_bkgPeakM(bkgPeakM_frac[0.09089, 0.,1.]*f_bkgPeakM1, f_bkgPeakM2)",

        "RooGaussian::f_bkgPeakM1(Bmass, bkgPeakM_mu[5.27, 5.25, 5.3], bkgPeakM_sigma1[0.01, 0.001, 1])",
        "RooGaussian::f_bkgPeakM2(Bmass, bkgPeakM_mu, bkgPeakM_sigma2[0.03, 0.001, 1])",
        "RooGaussian::f_bkgPeakM3(Bmass, bkgPeakM_mu, bkgPeakM_sigma3[0.04, 0.001, 1])",
        "SUM::f_bkgPeakM(bkgPeakM_frac1[0.01, 0.,1.]*f_bkgPeakM1, bkgPeakM_frac2[0.1, 0.,1.]*f_bkgPeakM2, f_bkgPeakM3)",

        "bkgParMshoulder[0.024, 0.024, 0.2]", "bkgParMdiv[5.148, 5.148, 5.22]",
        "EXPR::f_bkgParM('(TMath::Max(1*e-6, TMath::Erf((-Bmass + bkgParMshoulder)/bkgParMdiv ) + 1))', {args})".format(args="{Bmass, bkgParMshoulder, bkgParMdiv}"),

        "EXPR::f_bkgCombM('exp(bkgCombM_c1*Bmass)', {Bmass,bkgCombM_c1[-0.1,-0.1,0]})",
    ]
}
buildMassPDFs = functools.partial(buildGenericObj, **setupBuild_MassPDFs)

#---------------------------------------------------------------------------------------------------
setupBuild_DecayModels = {
    'objName': "DecayModels",
    'varNames': ["treco", "trecoe"],
    'factoryCmd' : [
        # "RooTruthModel::f_sigResolution(treco)",
        "RooGaussModel::f_sigResolution(treco, 0, 1, trecoe)",
        "RooDecay::f_sigD(treco, sigTtau[1.47, 1., 6.], f_sigResolution, RooDecay::SingleSided)",
        "RooEffProd::f_sigT(f_sigD, effi_sig)",

        "RooGaussModel::f_bkgPeakResolution(treco, 0, 1, trecoe)",
        "RooDecay::f_bkgPeakD(treco, bkgPeakTtau[1.55, 1, 2], f_bkgPeakResolution, RooDecay::SingleSided)",
        "RooEffProd::f_bkgPeakT(f_bkgPeakD, effi_peak)",

        "RooTruthModel::f_bkgParResolution(treco)",
        "RooDecay::f_bkgParT(treco, bkgParT1tau[1.45, 0, 5], f_bkgParResolution, RooDecay::SingleSided)",
        # "MyDecay::f_bkgParT(treco, bkgParT1tau[0.3, 0, 5], bkgParT2tau[0.4, 0, 5], bkgParTparam1[0.3, 0, 1], f_bkgParResolution, MyDecay::SingleSided)",

        "RooTruthModel::f_bkgCombResolution(treco)",
        "MyDecay::f_bkgCombT(treco, bkgCombT1tau[0.3, 0, 5], bkgCombT2tau[0.4, 0, 5], bkgCombTparam1[0.3, 0, 1], f_bkgCombResolution, MyDecay::SingleSided)",
    ]
}
buildDecayModels = functools.partial(buildGenericObj, **setupBuild_DecayModels)

#---------------------------------------------------------------------------------------------------
# DecayErrorModels PDFs
def initParam_DecayErrorModels(self):
    Year = self.process.cfg['args'].Year
    params = {}
    if Year == 2016: 
        params.update({
        'sigEgamma1': "sigEgamma1[1.71559e+01,3,27]", 'sigEgamma2': "sigEgamma2[1.10714e+01,4,27]",
        'sigEbeta1': "sigEbeta1[3.64880e-03,0.002,0.004]", 'sigEbeta2': "sigEbeta2[6.51664e-03, 0.002,0.01]",
        })
    if Year == 2017:
        params.update({
        'sigEgamma1': "sigEgamma1[6.85947e+00,3,27]", 'sigEgamma2': "sigEgamma2[1.28007e+01,4,27]", 'sigEgamma3': "sigEgamma3[6.85947e+00,3,27]",
        'sigEbeta1': "sigEbeta1[8.37011e-03,0.002,0.02]", 'sigEbeta2': "sigEbeta2[2.95771e-03, 0.002,0.01]", 'sigEbeta3': "sigEbeta3[2.95771e-03, 0.002,0.01]",
        'sigEmean1': 'sigEmean1[0.01, 0.00001, 0.1]', 'sigEmean2': 'sigEmean2[0.001, 0.00001, 0.1]',
        })
    if Year == 2018:
        params.update({
        'sigEgamma1': "sigEgamma1[1.23908e+01,3,27]", 'sigEgamma2': "sigEgamma2[8.30203e+00,4,27]", 'sigEgamma3': "sigEgamma3[6.85947e+00,3,27]",
        'sigEbeta1': "sigEbeta1[3.27683e-03,0.002,0.01]", 'sigEbeta2': "sigEbeta2[7.31354e-03, 0.002,0.01]", 'sigEbeta3': "sigEbeta3[2.95771e-03, 0.002,0.01]",
        'sigEmean1': 'sigEmean1[0.01, 0.00001, 0.1]', 'sigEmean2': 'sigEmean2[0.001, 0.00001, 0.1]',
        })
    return params

DecayErrorModels_format = {}
DecayErrorModels_format['default'] = [
        "RooGamma::f_sigE1(trecoe,sigEgamma1,sigEbeta1,0.00001)",
        "RooGamma::f_sigE2(trecoe,sigEgamma2,sigEbeta2,0.00001)",
        "SUM::f_sigE(sigEfrac[9.13468e-01, 0, 1]*f_sigE1, f_sigE2)"
        ]

DecayErrorModels_format['for17'] = [
        "RooJohnson::f_sigE1(trecoe, sigE_mu1[0.01, 1e-6, 0.2], sigE1_lambda[0.006, 1e-6, 1], sigE1_gamma[-7, -10, 10], sigE1_delta[3., 1e-6, 10])",
        "RooJohnson::f_sigE2(trecoe, sigE_mu1, sigE2_lambda[0.006, 1e-6, 1], sigE2_gamma[-7, -10, 10], sigE2_delta[3., 1e-6, 10])",
        "SUM::f_sigE(sigEfrac[9e-01, 0, 1]*f_sigE1, f_sigE2)"
        ]

DecayErrorModels_format['for18'] = [
        "RooJohnson::f_sigE1(trecoe, sigE_mu1[0.01, .0001, 0.2], sigE1_lambda[0.006, 1e-6, 1], sigE1_gamma[-7, -10, 10], sigE1_delta[3., 1e-6, 10])",
        "RooJohnson::f_sigE2(trecoe, sigE_mu1, sigE2_lambda[0.006, 1e-6, 1], sigE2_gamma[-7, -10, 10], sigE2_delta[3., 1e-6, 10])",
        "SUM::f_sigE(sigEfrac[9e-01, 0, 1]*f_sigE1, f_sigE2)"

        # "RooGaussModel::ErrorResolution(trecoe, sigE_mean[0.9, 0, 2], sigE_sigma[0.29, 0, 1], 0.03)",
        # "RooDecay::f_sigE(trecoe, sigE_tau[0.01, 0, 1], ErrorResolution, RooDecay::SingleSided)"

        ]

DecayErrorModels_format['TGamma'] = [
        "RooGamma::f_sigE1(trecoe,sigEgamma1,sigEbeta1,0.00424559)",
        "RooGamma::f_sigE2(trecoe,sigEgamma2,sigEbeta2,0.00424559)",
        "RooGamma::f_sigE3(trecoe,sigEgamma3,sigEbeta3,0.00424559)",
        "SUM::f_sigE(sigEfrac1[0.1, 0, 1]*f_sigE1, sigEfrac2[0.3, 0, 1]*f_sigE2, f_sigE3)"
]

DecayErrorModels_format['DGamma_FMean'] = [
        "RooGamma::f_sigE1(trecoe,sigEgamma1,sigEbeta1,sigEmean1)",
        "RooGamma::f_sigE2(trecoe,sigEgamma2,sigEbeta2,sigEmean2)",
        "SUM::f_sigE(sigEfrac[0.1, 0, 1]*f_sigE1, f_sigE2)"
        ]

def Build_DecayErrorModels(self):
    variables = initParam_DecayErrorModels(self)
    DecayErrorModels_format['default'] =  [variables[i] for i in variables]+DecayErrorModels_format['default']
    DecayErrorModels_format['TGamma'] = [variables[i] for i in variables]+DecayErrorModels_format['TGamma']
    DecayErrorModels_format['for18'] = [variables[i] for i in variables]+DecayErrorModels_format['for18']

    if self.process.cfg['args'].Year in [2017]:
        return DecayErrorModels_format.get(self.process.cfg['binKey'], DecayErrorModels_format['TGamma'])
    elif self.process.cfg['args'].Year in [2018]:
        return DecayErrorModels_format.get(self.process.cfg['binKey'], DecayErrorModels_format['TGamma'])
    else:
        return DecayErrorModels_format.get(self.process.cfg['binKey'], DecayErrorModels_format['default'])

setupBuild_DecayErrorModels = {
    'objName': "DecayErrorModels",
    'varNames': ["trecoe"],
    'factoryCmd' : [ ]
}

#---------------------------------------------------------------------------------------------------
def Build_PeakDecayErrorModels(self):
    # variables = initParam_DecayErrorModels(self)
    # copiedModel =  [variables[i] for i in variables]+DecayErrorModels_format['default']
    # copiedModel = [val.replace("sigE", "bkgPeakE") for val in copiedModel]
    copiedModel = [val.replace("sigE", "bkgPeakE") for val in Build_DecayErrorModels(self)]
    return copiedModel
setupBuild_PeakDecayErrorModels = {
    'objName': "PeakDecayErrorModels",
    'varNames': ["trecoe"],
    'factoryCmd' : [ ]
}

#---------------------------------------------------------------------------------------------------
def initParam_BkgCombDecayErrorModels(self):
    Year = self.process.cfg['args'].Year
    params = {}
    if Year == 2016: 
        params.update({
        'sigEgamma1': "[1.71559e+01,3,27]", 'sigEgamma2': "[1.10714e+01,4,27]",
        'sigEbeta1': "[3.64880e-03,0.002,0.01]", 'sigEbeta2': "[6.51664e-03, 0.002,0.01]",
        })
    if Year == 2017:
        params.update({
        'sigEgamma1': "[6.85947e+00,3,10]", 'sigEgamma2': "[1.28007e+01,4,15]",
        'sigEbeta1': "[8.37011e-02,0.002,0.01]", 'sigEbeta2': "[0.00395, 0.00038,0.009]",
        })
    if Year == 2018:
        params.update({
        'sigEgamma1': "[1.23908e+01,3,27]", 'sigEgamma2': "[8.30203e+00,4,27]",
        'sigEbeta1': "[3.27683e-03,0.002,0.01]", 'sigEbeta2': "[7.31354e-03, 0.002,0.01]",
        })
    return params
BkgCombDecayErrorModels_format = {}
BkgCombDecayErrorModels_format['default'] = [
        # "RooGamma::f_sigE1(trecoe,sigEgamma1,sigEbeta1,0.0002)",
        "RooGamma::f_sigE(trecoe,sigEgamma2,sigEbeta2,0.0002)",
        # "SUM::f_sigE(sigEfrac[2.13468e-01, 0, 1]*f_sigE1, f_sigE2)"
        ]

BkgCombDecayErrorModels_format['for16'] = [
        "RooJohnson::f_sigE(trecoe, sigE_mu1[0.01, 1e-6, 0.2], sigE1_lambda[0.006, 1e-6, 1], sigE1_gamma[-7, -10, 10], sigE1_delta[3., 1e-6, 10])"
        ]

BkgCombDecayErrorModels_format['for17'] = [
        "RooJohnson::f_sigE1(trecoe, sigE_mu1[0.01, 1e-6, 0.2], sigE1_lambda[0.006, 1e-6, 1], sigE1_gamma[-7, -10, 10], sigE1_delta[3., 1e-6, 10])",
        "RooJohnson::f_sigE2(trecoe, sigE_mu2[0.01, 1e-6, 0.2], sigE2_lambda[0.006, 1e-6, 1], sigE2_gamma[-7, -10, 10], sigE2_delta[3., 1e-6, 10])",
        "SUM::f_sigE(sigEfrac[9e-01, 0, 1]*f_sigE1, f_sigE2)"
        ]
BkgCombDecayErrorModels_format['for17v2'] = [
        "RooGaussian::f_sigE1(trecoe, sigE1_mean[0.04, 1e-3, 0.2], sigE1_sigma[0.01, 0.001, 1])",
        "RooGaussian::f_sigE2(trecoe, sigE2_mean[0.05, 1e-3, 0.2], sigE2_sigma[0.01, 0.001, 1])",
        "SUM::f_sigE(sigEfrac[7e-01, 0, 1]*f_sigE1, f_sigE2)"
        ]
BkgCombDecayErrorModels_format['for17v3'] = [
        "RooGaussian::f_sigE1(trecoe, sigE1_mean[0.05, 0.04, 0.06], sigE1_sigma[0.008, 0.005, 0.014])",
        "RooGaussian::f_sigE2(trecoe, sigE2_mean[0.04, 0.03, 0.05], sigE2_sigma[0.009, 0.005, 0.015])",
        "RooGaussian::f_sigE3(trecoe, sigE3_mean[0.06, 0.05, 0.06], sigE3_sigma[0.02, 0.01, 0.03])",
        "SUM::f_sigE(sigE1frac[0.3, 0, 1]*f_sigE1, sigE2frac[0.3, 0,1]*f_sigE2, f_sigE3)"
        ]

def Build_BkgCombDecayErrorModels(self):
    variables = initParam_BkgCombDecayErrorModels(self)
    BkgCombDecayErrorModels_format['default'] =  [i+variables[i] for i in variables]+BkgCombDecayErrorModels_format['default']
    # copiedModel = [val.replace("sigE", "bkgCombE") for val in BkgCombDecayErrorModels_format['default']]
    copiedModel = [val.replace("sigE", "bkgCombE") for val in BkgCombDecayErrorModels_format['for16']]
    return copiedModel
setupBuild_BkgCombDecayErrorModels = {
    'objName': "BkgCombDecayErrorModels",
    'varNames': ["trecoe"],
    'factoryCmd' : [ ]
}

#---------------------------------------------------------------------------------------------------
def Build_PartialDecayErrorModels(self):
    copiedModel = [val.replace("bkgCombE", "bkgParE") for val in Build_BkgCombDecayErrorModels(self)]
    return copiedModel
setupBuild_PartialDecayErrorModels = {
    'objName': "PartialDecayErrorModels",
    'varNames': ["trecoe"],
    'factoryCmd' : [ ]
}

#---------------------------------------------------------------------------------------------------
setupBuild_2DModels = {
    'objName': "2DModels",
    'varNames': ["treco", "trecoe"],
    'factoryCmd' : [
        "PROD::f_sig2D(f_sigM, f_sigT)",
        "PROD::f_bkgPeak2D(f_bkgPeakM, f_bkgPeakT)",
        "PROD::f_bkgComb2D(f_bkgCombM, f_bkgCombT)",
        "PROD::f_bkgPar2D(f_bkgParM, f_bkgParT)",
        # "SUM::f_final2D(frac1_final2D[0.2,0,1]*f_sig2D, frac2_final2D[0.2,0,1]*f_bkgPeak2D, frac3_final2D[0.2,0,1]*f_bkgComb2D, f_bkgPar2D)",

        "SUM::f_bkgM(frac_bkgM[0.3,0,1]*f_bkgCombM, f_bkgParM)",
        "SUM::f_bkgT(frac_bkgT[0.3,0,1]*f_bkgCombT, f_bkgParT)",
        "SUM::f_bkgE(frac_bkgE[0.3,0,1]*f_bkgCombE, f_bkgParE)",

        # For fixing Partial Bkg
        "PROD::f_bkgCombTE(f_bkgCombE, f_bkgCombT)",
        "PROD::f_bkgParTE(f_bkgParE, f_bkgParT)",        
        "SUM::f_bkg2D(frac_bkg2D[0.5,0,1]*f_bkgCombTE, f_bkgParTE)",

    ]
}
build2DModels = functools.partial(buildGenericObj, **setupBuild_2DModels)

#---------------------------------------------------------------------------------------------------
setupBuild_3DModels = {
    'objName': "3DModels",
    'varNames': ["treco", "trecoe"],
    'factoryCmd' : [
        "PROD::f_sig3D(f_sigT|trecoe, f_sigM, f_sigE)",
        "PROD::f_bkgPeak3D(f_bkgPeakT|trecoe, f_bkgPeakM, f_bkgPeakE)",
        "PROD::f_bkgComb3D(f_bkgCombM, f_bkgCombT, f_bkgCombE)",
        "PROD::f_bkgPar3D(f_bkgParM, f_bkgParT, f_bkgParE)",

        "SUM::f_bkg3D(nYield1[1500, 1e-5, 1e5]*f_bkgComb3D, nYield2[2300, 1e-5, 1e5]*f_bkgPar3D)",

        # "SUM::f_finalM(frac1_finalM[0.2, 0.,1.]*f_sigM, frac2_finalM[0.3, 0.,1.]*f_bkgPeakM, frac3_finalM[0.2, 0.,1.]*f_bkgCombM, f_bkgParM)",
        # "SUM::f_final3D(frac1_final3D[0.2, 0.,1.]*f_sig3D, frac2_final3D[0.3, 0.,1.]*f_bkgPeak3D, frac3_final3D[0.2, 0.,1.]*f_bkgComb3D, f_bkgPar3D)",
    ]
}
build3DModels = functools.partial(buildGenericObj, **setupBuild_3DModels)

#---------------------------------------------------------------------------------------------------
def buildFinal(self):
    """Combination of signal and background components."""
    wspace = self.getWspace()

    # Keep also mass spectrum only for prefit
    variations = [("f_finalM",  "f_sigM",  "f_bkgPeakM",  "f_bkgCombM",  "f_bkgParM"),
                  ("f_final2D", "f_sig2D", "f_bkgComb2D", "f_bkgPeak2D", "f_bkgPar2D"),
                  ("f_final3D", "f_sig3D", "f_bkgComb3D", "f_bkgPeak3D", "f_bkgPar3D")]
    wspace.factory("nSig[290,1e-2,1e8]")
    wspace.factory("nBkgComb[2200,1e-4,1e8]")
    wspace.factory("nBkgPeak[29600,1e-4,1e8]")
    wspace.factory("nBkgPar[1700,1e-4,1e8]")
    for p, pSig, pBkg, pBkgPeak, pBkgPar in variations:
        f_final = wspace.obj(p)
        if f_final == None:
            for k in [pSig, pBkg]:
                locals()[k] = self.cfg['source'][k] if k in self.cfg['source'] else self.process.sourcemanager.get(k)
                if wspace.obj(k) == None:
                    getattr(wspace, 'import')(locals()[k])
            wspace.factory("SUM::{0}(nSig*{1},nBkgComb*{2}, nBkgPeak*{3}, nBkgPar*{4})".format(p, pSig, pBkg, pBkgPeak, pBkgPar))
            f_final = wspace.obj(p)
        self.cfg['source'][p] = f_final
    wspace.Print()

sharedWspaceTagString = "{binLabel}"
CFG_WspaceReader = copy(WspaceReader.templateConfig())
CFG_WspaceReader.update({
    'obj': OrderedDict([
    ])  # Empty by default loads all Functions and Pdfs
})

#===================================================================================================
def customizeWspaceReader(self):
    self.cfg['fileName'] = "{0}/input/wspace_{2}_{1}.root".format(modulePath, tbins[self.process.cfg['binKey']]['label'], str(self.process.cfg['args'].Year))
    self.cfg['wspaceTag'] = sharedWspaceTagString.format(binLabel=tbins[self.process.cfg['binKey']]['label'])

def GetWspaceReader(self):
    stdWspaceReader = WspaceReader(CFG_WspaceReader); stdWspaceReader.name="stdWspaceReader.{0}".format(str(self.cfg['args'].Year))
    stdWspaceReader.customize = types.MethodType(customizeWspaceReader, stdWspaceReader)
    return stdWspaceReader

CFG_PDFBuilder = ObjProvider.templateConfig()
stdPDFBuilder = ObjProvider(copy(CFG_PDFBuilder)); stdPDFBuilder.name="stdPDFBuilder"
def customizePDFBuilder(self):
    """Customize pdf for decay time bins"""
    wspaceTag = sharedWspaceTagString.format(binLabel=tbins[self.process.cfg['binKey']]['label'])
    wspaceName = "wspace.{0}.{1}".format(self.process.cfg['args'].Year, wspaceTag)
    if wspaceName in self.process.sourcemanager.keys():
        Wspace = self.process.sourcemanager.get(wspaceName)
        if Wspace.allVars().find("treco"):
            self.logger.logDEBUG("RooWorkspace is already exists. Not building it again")
            return 0
        else:
            pass
    
    setupBuild_EfficiencyPDFs['factoryCmd'] = Build_EfficiencyPDFs(self)
    buildEfficiencyPDFs = functools.partial(buildGenericObj, **setupBuild_EfficiencyPDFs)

    setupBuild_DecayErrorModels['factoryCmd'] = Build_DecayErrorModels(self)
    buildDecayErrorModels = functools.partial(buildGenericObj, **setupBuild_DecayErrorModels)

    setupBuild_PeakDecayErrorModels['factoryCmd'] = Build_PeakDecayErrorModels(self)
    buildPeakDecayErrorModels = functools.partial(buildGenericObj, **setupBuild_PeakDecayErrorModels)

    setupBuild_BkgCombDecayErrorModels['factoryCmd'] = Build_BkgCombDecayErrorModels(self)
    buildBkgCombDecayErrorModels = functools.partial(buildGenericObj, **setupBuild_BkgCombDecayErrorModels)

    setupBuild_PartialDecayErrorModels['factoryCmd'] = Build_PartialDecayErrorModels(self)
    buildPartialDecayErrorModels = functools.partial(buildGenericObj, **setupBuild_PartialDecayErrorModels)

    # Configure setup
    self.cfg.update({
        'wspaceTag': wspaceTag,
        'obj': OrderedDict([
            ('Efficiencies', [buildEfficiencyPDFs]),
            ('MassPDFs', [buildMassPDFs]),
            ('DecayModels', [buildDecayModels]),
            ('DecayErrorModels', [buildDecayErrorModels]),
            ('PeakDecayErrorModels', [buildPeakDecayErrorModels]),
            ('BkgCombDecayErrorModels', [buildBkgCombDecayErrorModels]),
            ('PartialDecayErrorModels', [buildPartialDecayErrorModels]),
            ('2DModels', [build2DModels]),
            ('3DModels', [build3DModels]),
            ('f_final', [buildFinal]),  # Include all variations
        ])
    })
stdPDFBuilder.customize = types.MethodType(customizePDFBuilder, stdPDFBuilder)

if __name__ == '__main__':
    pass
