#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set sw=4 ts=4 fdm=indent fdl=2 ft=python et:

# Author          :  Pritam Kalbhor (physics.pritam@gmail.com)

from __future__ import print_function, division
import os
from math import sqrt

# Shared global settings
modulePath = os.path.abspath(os.path.dirname(__file__))

# Lifetime bins
tbins = {}
def createBinTemplate(name, lowerBd, upperBd):
    template = {
        'q2range': (lowerBd, upperBd),
        'cutString': "treco > {0} && treco < {1}".format(lowerBd, upperBd),
        'label': "{0}".format(name),
        'latexLabel': "{lowerBd:.2f} < q^{{2}} < {upperBd:.2f}".format(upperBd=upperBd, lowerBd=lowerBd),
    }
    return template

tbins['full'] = createBinTemplate("full", .0, 10)

# B mass regions
bMassRegions = {}
def createBmassTemplate(name, lowerBd, upperBd):
    template = {
        'range': (lowerBd, upperBd),
        'cutString': "Bmass > {0} && Bmass < {1}".format(lowerBd, upperBd),
        'label': "{0}".format(name),
    }
    return template

bMassRegions['Full'] = createBmassTemplate("Full", 4.7, 6.0) # Cut off below 4.68
bMassRegions['Fit']  = createBmassTemplate("Fit",  5.1, 5.6)
bMassRegions['SR']   = createBmassTemplate("SR",   5.25, 5.45) #Signal Region
bMassRegions['LSB']  = createBmassTemplate("LSB",  5.1, 5.17) #("LSB", 5.143, 5.223)
bMassRegions['USB']  = createBmassTemplate("USB",  5.45, 6.0) #("USB", 5.511, 5.591)
bMassRegions['SB']   = createBmassTemplate("SB",   5.1, 6.0)
bMassRegions['NSB']   = createBmassTemplate("NSB", 5.17, 5.45)
bMassRegions['SB']['cutString'] = "({0}) && !({1})".format(bMassRegions['SB']['cutString'], bMassRegions['NSB']['cutString'])

# systematics
bMassRegions['altFit'] = createBmassTemplate("altFit", 5.1, 5.60)
bMassRegions['altSR']  = createBmassTemplate("altSR", 5.25, 5.45)
bMassRegions['altLSB'] = createBmassTemplate("altLSB", 5.1, 5.25)
bMassRegions['altUSB'] = createBmassTemplate("altUSB", 5.45, 5.60)
bMassRegions['altSB']  = createBmassTemplate("altSB", 5.10, 5.60)
bMassRegions['altSB']['cutString'] = "({0}) && !({1})".format(bMassRegions['altSB']['cutString'], bMassRegions['altSR']['cutString'])

bMassRegions['altFit_vetoJpsiX'] = createBmassTemplate("altFit_vetoJpsiX", 5.18, 5.80)
bMassRegions['altSR_vetoJpsiX']  = createBmassTemplate("altSR_vetoJpsiX", 5.18, 5.38)
bMassRegions['altLSB_vetoJpsiX'] = createBmassTemplate("altLSB_vetoJpsiX", 5.18, 5.18)
bMassRegions['altUSB_vetoJpsiX'] = createBmassTemplate("altUSB_vetoJpsiX", 5.38, 5.80)
bMassRegions['altSB_vetoJpsiX']  = createBmassTemplate("altSB_vetoJpsiX", 4.76, 5.80)
bMassRegions['altSB_vetoJpsiX']['cutString'] = "({0}) && !({1})".format(bMassRegions['altSB_vetoJpsiX']['cutString'], bMassRegions['altSR_vetoJpsiX']['cutString'])

