#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set sw=4 sts=4 fdm=indent fdl=0 fdn=3 ft=python et:

import types
from copy import deepcopy

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
import LifetimeFitter.cpp
from v2Fitter.Fitter.FitterCore import FitterCore
from LifetimeFitter.StdFitter import StdFitter, flToUnboundFl, afbToUnboundAfb
from LifetimeFitter.EfficiencyFitter import EfficiencyFitter

from LifetimeFitter.StdProcess import p
import LifetimeFitter.dataCollection as dataCollection
import LifetimeFitter.pdfCollection as pdfCollection
from LifetimeFitter.anaSetup import bMassRegions
from LifetimeFitter.varCollection import Bmass, treco, trecoe

setupTemplateFitter = StdFitter.templateConfig()

def sigAFitter_bookPdfData(self):
    self.process.dbplayer.saveSMPrediction()
    StdFitter._bookPdfData(self)
    self.data.changeObservableName("genCosThetaK", "CosThetaK")
    self.data.changeObservableName("genCosThetaL", "CosThetaL")

ArgAliasRECO={'sigTtau': 'sigTtau_RECO'}
ArgAliasGEN ={'unboundAfb': 'unboundAfb_GEN', 'unboundFl': 'unboundFl_GEN'}
ArgAliasSigM={'sigMGauss1_sigma': 'sigMGauss1_sigma_RECO', 'sigMGauss2_sigma': 'sigMGauss2_sigma_RECO', 'sigMGauss_mean': 'sigMGauss_mean_RECO', 'sigM_frac': 'sigM_frac_RECO'}
ArgAliasDCB ={'cbs1_sigma': 'cbs1_sigma_RECO', 'cbs2_sigma': 'cbs2_sigma_RECO', 'cbs1_alpha': 'cbs1_alpha_RECO', 'cbs2_alpha': 'cbs2_alpha_RECO', 'cbs1_n': 'cbs1_n_RECO', 'cbs2_n': 'cbs2_n_RECO', 'cbs_mean': 'cbs_mean_RECO', 'sigMDCB_frac': 'sigMDCB_frac_RECO'}
ArgAliasJP = {}
ArgAliasJK = {}
ArgAlias_PhiM_JP = {'Phicbs1_alpha': 'Phicbs1_alpha_MC', 'Phicbs1_n':'Phicbs1_n_MC', 'Phicbs1_sigma':'Phicbs1_sigma_MC', 
        'Phicbs2_alpha':'Phicbs2_alpha_MC', 'Phicbs2_n':'Phicbs2_n_MC', 'Phicbs2_sigma':'Phicbs2_sigma_MC', 
        'Phicbs_mean':'Phicbs_mean_MC', 'sigPhiM_DCB_frac':'sigPhiM_DCB_frac_MC', 
        'sigPhiM_TCB_frac1':'sigPhiM_TCB_frac1_MC', 'sigPhiM_TCB_frac2':'sigPhiM_TCB_frac2_MC',
        'Phicbs3_alpha': 'Phicbs3_alpha_MC', 'Phicbs3_n':'Phicbs3_n_MC', 'Phicbs3_sigma':'Phicbs3_sigma_MC',
        'sigPhiMG_mean':'sigPhiMG_mean_MC', 'sigPhiM_frac':'sigPhiM_frac_MC', 
        'sigPhiMG1_sigma':'sigPhiMG1_sigma_MC', 'sigPhiMG2_sigma':'sigPhiMG2_sigma_MC', 'sigPhiMG3_sigma':'sigPhiMG3_sigma_MC', 
        'sigPhiM3_frac1':'sigPhiM3_frac1_MC', 'sigPhiM3_frac2':'sigPhiM3_frac2_MC'}

def GetFitterObjects(self, seq):
    Year=self.cfg['args'].Year
    AltRange = self.cfg['args'].AltRange
    binKey = self.cfg['binKey']
    TwoStep = self.cfg['args'].TwoStep
    if seq =='sigEffiFitter':
        setupEffiFitter = deepcopy(EfficiencyFitter.templateConfig())
        setupEffiFitter.update({
            'name'  : "sigEffiFitter.{0}".format(Year),
            'label' : "_sig",
            'datahist': 'Eff_sigHist_{0}'.format(Year), # TH1D
            'data'  : 'Eff_sig_{0}'.format(Year),#1D RooDataHist
            'pdf'   : 'effi_sig.{}'.format(Year),
        })
        return EfficiencyFitter(setupEffiFitter)
    if seq =='peakEffiFitter':
        setupEffiFitter = deepcopy(EfficiencyFitter.templateConfig())
        setupEffiFitter.update({
            'name'  : "peakEffiFitter.{0}".format(Year),
            'label' : "_peak",
            'datahist': 'Eff_peakHist_{0}'.format(Year), # TH1D
            'data'  : 'Eff_peak_{0}'.format(Year),#1D RooDataHist
            'pdf'   : 'effi_peak.{}'.format(Year),
        })
        return EfficiencyFitter(setupEffiFitter)
    if seq == 'effiFitter2':
        setupEffiFitter = deepcopy(setupTemplateFitter)
        setupEffiFitter.update({
            'name'  : "effiFitter.{0}".format(Year),
            'label' : '',
            'data'  : 'Eff_sig_{0}'.format(Year),
            'pdf'   : 'effi_sig.{}'.format(Year),
            'createNLLOpt': [ROOT.RooFit.Range("dtRange")],
        })
        return StdFitter(setupEffiFitter)
    if seq == 'sigMFitter':
        setupSigMFitter = deepcopy(setupTemplateFitter)
        setupSigMFitter.update({
            'name': "sigMFitter.{}".format(Year),
            'data': "sigMCReader.{}.Fit".format(Year),
            'pdf': "f_sigM.{}".format(Year),
            'createNLLOpt': [ROOT.RooFit.Range("BsfitRange"),],
        })
        return StdFitter(setupSigMFitter)
    if seq == 'sigTFitter':
        setupSigTFitter = deepcopy(setupTemplateFitter)
        setupSigTFitter.update({
            'name': "sigTFitter.{}".format(Year),
            'data': "sigMCReader.{}.Fit".format(Year),
            'pdf': "f_sigT.{}".format(Year),
            'argPattern': ['sigTtau'],
            'argAliasInDB': {'sigTtau': 'sigTtau_RECO'},
            'FitMinos': [True, ('sigTtau')],
            'createNLLOpt': [ROOT.RooFit.Range("dtRange"), ROOT.RooFit.ConditionalObservables(trecoe)],
        })
        return StdFitter(setupSigTFitter)
    if seq == 'sigEFitter':
        setupSigEFitter = deepcopy(setupTemplateFitter)
        setupSigEFitter.update({
            'name': "sigEFitter.{}".format(Year),
            'data': "sigMCReader.{}.Fit".format(Year),
            'pdf': "f_sigE.{}".format(Year),
            'createNLLOpt': [ROOT.RooFit.Range("dtErrRange")],
        })
        return StdFitter(setupSigEFitter)
    if seq == 'peakMFitter':
        setupPeakMFitter = deepcopy(setupTemplateFitter)
        setupPeakMFitter.update({
            'name': "peakMFitter.{}".format(Year),
            'data': "peakMCReader.{}.Fit".format(Year),
            'pdf': "f_bkgPeakM.{}".format(Year),
            'createNLLOpt': [ROOT.RooFit.Range("BdfitRange"),],
        })
        return StdFitter(setupPeakMFitter)
    if seq == 'peakTFitter':
        setupPeakTFitter = deepcopy(setupTemplateFitter)
        setupPeakTFitter.update({
            'name': "peakTFitter.{}".format(Year),
            'data': "peakMCReader.{}.Fit".format(Year),
            'pdf': "f_bkgPeakT.{}".format(Year),
            'argPattern': ['bkgPeakTtau'],
            'argAliasInDB': {'bkgPeakTtau': 'bkgPeakTtau_RECO'},
            'FitMinos': [True, ('bkgPeakTtau')],
            'createNLLOpt': [ROOT.RooFit.Range("dtRange"), ROOT.RooFit.ConditionalObservables(ROOT.RooArgSet(trecoe))],
        })
        return StdFitter(setupPeakTFitter)
    if seq == 'peakEFitter':
        setupPeakEFitter = deepcopy(setupTemplateFitter)
        setupPeakEFitter.update({
            'name': "peakEFitter.{}".format(Year),
            'data': "peakMCReader.{}.Fit".format(Year),
            'pdf': "f_bkgPeakE.{}".format(Year),   
            'createNLLOpt': [ROOT.RooFit.Range("dtErrRange")],
        })
        return StdFitter(setupPeakEFitter)
    if seq == 'peak3DFitter':
        setupPeak3DFitter = deepcopy(setupTemplateFitter)
        setupPeak3DFitter.update({
            'name': "peak3DFitter.{}".format(Year),
            'data': "peakMCReader.{}.Fit".format(Year),
            'pdf': "f_bkgPeak3D.{}".format(Year),
            'argPattern': [r'^bkgPeakM.*', r'^bkgPeakT.*', r'^bkgPeakE.*'],
            'argAliasInDB': {'bkgPeakTtau': 'bkgPeakTtau_RECO'},
            'argAliasFromDB': {'bkgPeakTtau': 'bkgPeakTtau_RECO'},
            # 'createNLLOpt': [ROOT.RooFit.Range("dtRange"), ROOT.RooFit.ConditionalObservables(trecoe)],
            'fitToOpt': [True, (ROOT.RooFit.Range("dtRange"), ROOT.RooFit.SumW2Error(True), ROOT.RooFit.Strategy(2), ROOT.RooFit.PrintLevel(3))],
        })
        return StdFitter(setupPeak3DFitter)        
    if seq == 'sig2DFitter':
        setupSig2DFitter = deepcopy(setupTemplateFitter)
        setupSig2DFitter.update({
            'name': "sig2DFitter.{}".format(Year),
            'data': "sigMCReader.{}.Fit".format(Year),
            'pdf': "f_sig2D.{}".format(Year),
            'FitHesse': True,
            'FitMinos': [True, ()],
            'argPattern': ['tau_Sig', 'frac_sig2D', r'^sigM.*', 'bias', 'sigma'],
            'createNLLOpt': [],
        })
        return StdFitter(setupSig2DFitter)
    if seq == 'bkgCombTFitter':
        setupBkgCombTFitter = deepcopy(setupTemplateFitter)
        setupBkgCombTFitter.update({
            'name': "bkgCombTFitter.{}".format(Year),
            'data': "dataReader.{}.USB".format(Year),
            'pdf': "f_bkgCombT.{}".format(Year),
            'createNLLOpt': [ROOT.RooFit.Range("dtRange")],
        })
        return StdFitter(setupBkgCombTFitter)
    if seq == 'bkgCombEFitter':
        setupBkgCombEFitter = deepcopy(setupTemplateFitter)
        setupBkgCombEFitter.update({
            'name': "bkgCombEFitter.{}".format(Year),
            'data': "dataReader.{}.USB".format(Year),
            'pdf': "f_bkgCombE.{}".format(Year),   
            'createNLLOpt': [ROOT.RooFit.Range("dtErrRange")],
        })
        return StdFitter(setupBkgCombEFitter)
    if seq == 'bkgParTFitter':
        setupBkgParTFitter = deepcopy(setupTemplateFitter)
        setupBkgParTFitter.update({
            'name': "bkgParTFitter.{}".format(Year),
            'data': "dataReader.{}.LSB".format(Year),
            'pdf': "f_bkgT.{}".format(Year),
            'argPattern': ['frac_bkgT', r'^bkgParT.*'],
            'createNLLOpt': [ROOT.RooFit.Range("dtRange")],
        })
        return StdFitter(setupBkgParTFitter)
    if seq == 'bkgParEFitter':
        setupBkgParEFitter = deepcopy(setupTemplateFitter)
        setupBkgParEFitter.update({
            'name': "bkgParEFitter.{}".format(Year),
            'data': "dataReader.{}.LSB".format(Year),
            'pdf': "f_bkgE.{}".format(Year),
            'argPattern': ['frac_bkgE', r'^bkgParE.*'],
            'createNLLOpt': [ROOT.RooFit.Range("dtErrRange")],
        })
        return StdFitter(setupBkgParEFitter)
    if seq == 'bkg3DFitter':
        """For fixing semileptonic background shapes"""
        setupBkg3DFitter = deepcopy(setupTemplateFitter)
        setupBkg3DFitter.update({
            'name': "bkg3DFitter.{}".format(Year),
            'data': "dataReader.{}.SB".format(Year),
            'pdf': "f_bkg3D.{}".format(Year),
            'argPattern': [r'^bkgCombM.*', r'^bkgParM.*', r'^bkgParT.*', r'^bkgParE.*', 'nYield1', 'nYield2'],
            'createNLLOpt': [ROOT.RooFit.Range("bmass1,bmass2")],
        })
        return StdFitter(setupBkg3DFitter)
    if seq == 'sig3DFitter':
        setupSig3DFitter = deepcopy(setupTemplateFitter)
        setupSig3DFitter.update({
            'name': "sig3DFitter.{}".format(Year),
            'data': "sigMCReader.{}.Fit".format(Year),
            'pdf': "f_sig3D.{}".format(Year),
            'argPattern': [r'^sigM.*', r'^sigT.*', r'^sigE.*'],
            'argAliasInDB': {'sigTtau': 'sigTtau_RECO'},
            'argAliasFromDB': {'sigTtau': 'sigTtau_RECO'},
            # 'createNLLOpt': [ROOT.RooFit.Range("dtRange"), ROOT.RooFit.ConditionalObservables(trecoe)],
            'fitToOpt': [True, (ROOT.RooFit.Range("dtRange"), ROOT.RooFit.SumW2Error(True), ROOT.RooFit.Strategy(2), ROOT.RooFit.PrintLevel(3))],
        })
        return StdFitter(setupSig3DFitter)
    if seq == 'final3DFitter':
        setupFinal3DFitter = deepcopy(setupTemplateFitter)                         # 3D = nSig(Sig3D) + nBkg(fBkgM*fBkgA) + nBkgPeak(fBkgPeak)
        setupFinal3DFitter.update({
            'name': "final3DFitter.{}".format(Year),
            'data': "dataReader.{}.Fit".format(Year),
            'pdf': "f_final3D.{}".format(Year),
            'argPattern': ['nSig', 'nBkgComb', 'nBkgPeak', 'nBkgPar', 'sigTtau', 'bkgPeakTtau', 'bkgParMshoulder', 'bkgParMdiv', r'bkgCombM_c[\d]+'],
            'createNLLOpt': [ROOT.RooFit.Extended(True), ROOT.RooFit.Range("dtRange"), ROOT.RooFit.ConditionalObservables(trecoe)],
            'FitHesse': True,
            'FitMinos': [False, ('nSig')],
            'argAliasFromDB': {'sigTtau': 'sigTtau_RECO', 'bkgPeakTtau': 'bkgPeakTtau_RECO'},
            'argAliasSaveToDB': False,
            'saveToDB': False,
        })
        return StdFitter(setupFinal3DFitter)
    if seq is 'finalMFitter':
        setupFinalMFitter = deepcopy(setupTemplateFitter)               # Final Mass PDF = nSig(SigM)+nBkg(fBkgM)+nBkgP(fBkgP)
        setupFinalMFitter.update({
            'name': "finalMFitter.{}".format(Year),
            'data': "dataReader.{}.Fit".format(Year),
            'pdf': "f_finalM.{}".format(Year),
            'argPattern': ['nSig', 'nBkgComb', 'nBkgPeak', r'bkgCombM_c[\d]+'],
            'createNLLOpt': [ROOT.RooFit.Extended(True), ],
            'FitMinos': [True, ('nSig', 'nBkgComb', 'nBkgPeak')],
        })
        return StdFitter(setupFinalMFitter)


setupSigGENFitter = deepcopy(setupTemplateFitter) #Unimplemented
setupSigGENFitter.update({
    'name': "sigGENFitter",
    'data': "sigMCGENReader.Fit",
    'pdf': "f_sigA",
    'argPattern': ['unboundAfb', 'unboundFl'],
    'createNLLOpt': [],
    'argAliasInDB': {'unboundAfb': 'unboundAfb_GEN', 'unboundFl': 'unboundFl_GEN'},
})
sigGENFitter = StdFitter(setupSigGENFitter)

setupBkgCombMFitter = deepcopy(setupTemplateFitter)
setupBkgCombMFitter.update({
    'name': "bkgCombMFitter",
    'data': "dataReader.SB",
    'pdf': "f_bkgCombM",
    'argPattern': [r'bkgCombM_c[\d]+', ],
    'FitHesse': False,
    'FitMinos': [False, ()],
    'createNLLOpt': [],
})
bkgCombMFitter = StdFitter(setupBkgCombMFitter)

## Fitter Objects for SimultaneousFitter
from v2Fitter.Fitter.SimultaneousFitter import SimultaneousFitter
setupTemplateSimFitter = SimultaneousFitter.templateConfig()

setupSimultaneousFitter = deepcopy(setupTemplateSimFitter)
setupSimultaneousFitter.update({
    'category'  : ['cat16', 'cat17', 'cat18'],
    'data'      : ["sigMCReader.2016.Fit", "sigMCReader.2017.Fit", "sigMCReader.2018.Fit"],
    'pdf'       : ["f_sig2D_ts.2016", "f_sig2D_ts.2017", "f_sig2D_ts.2018"],
    'argPattern': ['unboundAfb', 'unboundFl'],
    'argAliasInDB': ArgAliasRECO,
    'LegName'  : 'Simulation',
    'fitToCmds' : [[ROOT.RooFit.Strategy(2), ROOT.RooFit.InitialHesse(1), ROOT.RooFit.Minimizer('Minuit', 'minimize'), ROOT.RooFit.Minos(1)],],
})
SimultaneousFitter_sig2D = SimultaneousFitter(setupSimultaneousFitter)

# 3D Signal fit
setupeSig3DFitter = deepcopy(setupTemplateSimFitter)
setupeSig3DFitter.update({
    'category'  : ['cat16', 'cat17', 'cat18'],
    'data'      : ["sigMCReader.2016.Fit", "sigMCReader.2017.Fit", "sigMCReader.2018.Fit"],
    'pdf'       : ["f_sig3D_ts.2016", "f_sig3D_ts.2017", "f_sig3D_ts.2018"],
    'argPattern': ['unboundAfb', 'unboundFl'],
    'Years'     : [2016, 2017, 2018],
    'argAliasInDB': ArgAliasRECO,
    'LegName'  : 'Simulation',
    'fitToCmds' : [[ROOT.RooFit.Strategy(2), ROOT.RooFit.InitialHesse(1), ROOT.RooFit.Minimizer('Minuit', 'minimize'), ROOT.RooFit.Minos(1)],],
})
SimultaneousFitter_sig3D = SimultaneousFitter(setupeSig3DFitter)

# sigMC Validation
Simul_sigMCValidation_setup = deepcopy(setupTemplateSimFitter)
Simul_sigMCValidation_setup.update({
    'category'  : ['cat16', 'cat17', 'cat18'],
    'data'      : ["sigMCReader.2016.Fit", "sigMCReader.2017.Fit", "sigMCReader.2018.Fit"],
    'pdf'       : ["f_sig3D_ts.2016", "f_sig3D_ts.2017", "f_sig3D_ts.2018"],
    'argPattern': ['unboundAfb', 'unboundFl'],
    'Years'     : [2016, 2017, 2018],
    'argAliasInDB': ArgAliasRECO,
    'LegName'   : 'Simulation',
    'fitToCmds' : [[ROOT.RooFit.Strategy(2), ROOT.RooFit.InitialHesse(1), ROOT.RooFit.Minimizer('Minuit', 'minimize'), ROOT.RooFit.Minos(1)],],
})
SimultaneousFitter_sigMCValidation = SimultaneousFitter(Simul_sigMCValidation_setup)

# Privat MC fit
setupSimulFitter_sigGEN = deepcopy(setupTemplateSimFitter)
setupSimulFitter_sigGEN.update({
    'category'  : ['cat16', 'cat17', 'cat18'],
    'data'      : ["sigMCGENReader.2016.Fit", "sigMCGENReader.2017.Fit", "sigMCGENReader.2018.Fit"],
    'pdf'       : ["f_sigA.2016", "f_sigA.2017", "f_sigA.2018"],
    'argPattern': ['unboundAfb', 'unboundFl'],
    'Years'     : [2016, 2017, 2018],
    'LegName'  : 'Simulation',
    'argAliasInDB': ArgAliasGEN,
    'fitToCmds' : [[ROOT.RooFit.Strategy(2), ROOT.RooFit.Minimizer('Minuit', 'minimize'), ROOT.RooFit.Minos(1)], ],
})
SimulFitter_sigGEN = SimultaneousFitter(setupSimulFitter_sigGEN)

#Final Fit
setupSimFinalFitter_WithKStar = deepcopy(setupTemplateFitter)                 #3D = nSig(Sig3D) + nBkg(fBkgM*fBkgA)
setupSimFinalFitter_WithKStar.update({
    'name'      : 'SimultFitterFinal',
    'category'  : ['cat16', 'cat17', 'cat18'],
    'data'      : ["dataReader.2016.Fit", "dataReader.2017.Fit", "dataReader.2018.Fit"],
    'pdf'       : ["f_final_WithKStar_ts.2016", "f_final_WithKStar_ts.2017", "f_final_WithKStar_ts.2018"],
    'argPattern': ['unboundAfb', 'unboundFl', 'nBkgComb_2016', 'nBkgComb_2017', 'nBkgComb_2018', 
                    'nSig_2016', 'nSig_2017', 'nSig_2018', 'bkgCombM_c1_2016', 'bkgCombM_c1_2017', 'bkgCombM_c1_2018'],
    'Years'     : [2016, 2017, 2018],
    'argAliasInDB': {**ArgAliasGEN},
    'LegName'   : 'Data',
    'argAliasSaveToDB': False,
    'fitToCmds' : [[ROOT.RooFit.Strategy(2), ROOT.RooFit.InitialHesse(1), ROOT.RooFit.Minimizer('Minuit', 'minimize'), ROOT.RooFit.Minos(0)],],
})
SimFitter_Final_WithKStar = SimultaneousFitter(setupSimFinalFitter_WithKStar)

# mixToy Validation
Simul_mixedToyValidation_setup = deepcopy(setupTemplateFitter)                 #3D = nSig(Sig2D*SigMDCB) + nBkg(fBkgM*fBkgA)
Simul_mixedToyValidation_setup.update({
    'name'      : 'SimultFitterMixedToy',
    'category'  : ['cat16', 'cat17', 'cat18'],
    'data'      : ["dataReader.2016.Fit", "dataReader.2017.Fit", "dataReader.2018.Fit"],
    'pdf'       : ["f_final_WithKStar_ts.2016", "f_final_WithKStar_ts.2017", "f_final_WithKStar_ts.2018"],
    'argPattern': ['unboundAfb', 'unboundFl', 'nBkgComb_2016', 'nBkgComb_2017', 'nBkgComb_2018', 'nSig_2016', 'nSig_2017', 'nSig_2018'], #r'nBkgComb*.', r'nSig*.'], # 'bkgCombM_c1_2016', 'bkgCombM_c1_2017', 'bkgCombM_c1_2018'],
    'Years'     : [2016, 2017, 2018],
    'argAliasInDB': {**ArgAliasGEN},
    'LegName'   : 'Data',
    'argAliasSaveToDB': False,
    'fitToCmds' : [[ROOT.RooFit.Strategy(3), ROOT.RooFit.InitialHesse(0), ROOT.RooFit.Minimizer('Minuit2', 'migrad'), ROOT.RooFit.Minos(0)],],
})
SimultaneousFitter_mixedToyValidation = SimultaneousFitter(Simul_mixedToyValidation_setup)

if __name__ == '__main__':
    p.setSequence([dataCollection.effiHistReader, pdfCollection.stdWspaceReader, effiFitter])
    #  p.setSequence([dataCollection.sigMCReader, pdfCollection.stdWspaceReader, sigMFitter])
    #  p.setSequence([dataCollection.sigMCReader, pdfCollection.stdWspaceReader, sig2DFitter])
    #  p.setSequence([dataCollection.dataReader, pdfCollection.stdWspaceReader, bkgCombAFitter])
    p.beginSeq()
    p.runSeq()
    p.endSeq()
