#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set sw=4 sts=4 fdm=indent fdl=0 fdn=3 ft=python et:

import os, sys, pdb, copy, re, math, types, functools, shelve 
from array import array
from math import sqrt
import ROOT
from ROOT import RooFit
from ROOT.RooFit import Range, NormRange, Normalization, ProjectionRange, Components
ROOT.PyConfig.IgnoreCommandLineOptions = True
import LifetimeFitter.cpp
from v2Fitter.FlowControl.Path import Path
from v2Fitter.Fitter.FitterCore import FitterCore
from LifetimeFitter.anaSetup import tbins, modulePath, bMassRegions
from LifetimeFitter.StdFitter import unboundFlToFl, unboundAfbToAfb, flToUnboundFl, afbToUnboundAfb
from LifetimeFitter.FitDBPlayer import FitDBPlayer
from LifetimeFitter.varCollection import Bmass, trecoe, treco

from LifetimeFitter.StdProcess import p, setStyle
import LifetimeFitter.dataCollection as dataCollection
import LifetimeFitter.pdfCollection as pdfCollection
import LifetimeFitter.fitCollection as fitCollection

from LifetimeFitter.Plotter import Plotter, defaultPlotRegion, plotterCfg_styles


def plotSpectrumWithSimpleFit(self, pltName, dataPlots, marks):
    """ Assuming len(dataPlots) == 1, fit to the data. """
    for pIdx, plt in enumerate(dataPlots):
        dataPlots[pIdx] = self.initDataPlotCfg(plt)
    wspace = ROOT.RooWorkspace("wspace")
    getattr(wspace, 'import')(Bmass)
    wspace.factory("RooGaussian::gauss1(Bmass,mean[5.28,5.25,5.39],sigma1[0.02,0.01,0.05])")
    wspace.factory("RooGaussian::gauss2(Bmass,mean,sigma2[0.08,0.05,0.40])")
    wspace.factory("SUM::sigM(sigFrac[0.8,0,1]*gauss1,gauss2)")
    wspace.factory("c1[-5.6,-30,30]")
    wspace.factory("EXPR::bkgCombM('exp(c1*Bmass)',{Bmass,c1})")
    wspace.factory("SUM::model(tmp_nSig[1,1e5]*sigM,tmp_nBkg[20,1e5]*bkgCombM)")
    wspace.factory("cbs_mean[5.28, 5.25, 5.39]")
    wspace.factory("RooCBShape::cbs_1(Bmass, cbs_mean, cbs1_sigma[0.8, 0.0001, 0.60], cbs1_alpha[0.8, -6.0, 6.0], cbs1_n[10, 0, 1000])")
    wspace.factory("RooCBShape::cbs_2(Bmass, cbs_mean, cbs2_sigma[0.005, 0.0001, 0.60], cbs2_alpha[-0.8, -2.4, 5.6], cbs2_n[100, 0, 1000])")
    wspace.factory("SUM::f_sigMDCB(sigMDCB_frac[0.8,0, 1]*cbs_1, cbs_2)")
    wspace.factory("SUM::model2(tmp_nSig*f_sigMDCB,tmp_nBkg*bkgCombM)")
    
    if False:
        pdfPlots = [
            [wspace.pdf('model'), plotterCfg_allStyle, None, "Total fit"],
            [wspace.pdf('model'), (ROOT.RooFit.Components('sigM'),) + plotterCfg_sigStyle, None, "Signal"],
            [wspace.pdf('model'), (ROOT.RooFit.Components('bkgCombM'),) + plotterCfg_bkgStyle, None, "Background"],
        ]
    else:
         pdfPlots = [
            [wspace.pdf('model2'), plotterCfg_allStyle, None, "Total fit"],
            [wspace.pdf('model2'), (ROOT.RooFit.Components('f_sigMDCB'),) + plotterCfg_sigStyle, None, "Signal"],
            [wspace.pdf('model2'), (ROOT.RooFit.Components('bkgCombM'),) + plotterCfg_bkgStyle, None, "Background"],
        ]
   
    fitter=pdfPlots[0][0].fitTo(dataPlots[0][0], Range("signal"), ROOT.RooFit.Minos(True), ROOT.RooFit.Extended(True), ROOT.RooFit.PrintLevel(-1), ROOT.RooFit.Save(True))
    fitter.Print("v")
    Plotter.plotFrameB(dataPlots=dataPlots, pdfPlots=pdfPlots, marks=marks)
    self.canvasPrint(pltName)
types.MethodType(plotSpectrumWithSimpleFit, Plotter)

def plotSimpleBLK(self, pltName, dataPlots, pdfPlots, marks, frames='BLK'):
    for pIdx, plt in enumerate(dataPlots):
        dataPlots[pIdx] = self.initDataPlotCfg(plt)
    for pIdx, plt in enumerate(pdfPlots):
        pdfPlots[pIdx] = self.initPdfPlotCfg(plt)

    plotFuncs = {
        'B': {'func': Plotter.plotFrameB_fine, 'tag': "_Bmass"},
        'L': {'func': Plotter.plotFrameL, 'tag': "_dt"},
        'K': {'func': Plotter.plotFrameK, 'tag': "_dtErr"},
    }
    cwd=os.getcwd()
    for frame in frames:
        NoFit= self.process.cfg['args'].NoFit
        plotFuncs[frame]['func'](dataPlots=dataPlots, pdfPlots=pdfPlots, marks=marks, legend=False if NoFit else True, Plotpdf=(not NoFit), NoPull=self.process.cfg['args'].NoPull, self=self)
        plotName = pltName.strip('.{}'.format(self.process.cfg['args'].Year)).strip()
        if plotName in ['plot_sigT', 'plot_sig3D', 'plot_peakT'] and frame=='L':
            variable = "sigTtau" if 'plot_sig' in plotName else 'bkgPeakTtau'
            Plotter.latex.DrawLatexNDC(.45, .8, r"#scale[0.8]{{#tau = {0:.3f}}}".format(pdfPlots[0][0].getParameters(dataPlots[0][0]).find(variable).getVal()) )
            tauErrorLo = abs(pdfPlots[0][0].getParameters(dataPlots[0][0]).find(variable).getErrorLo())
            tauErrorHi = pdfPlots[0][0].getParameters(dataPlots[0][0]).find(variable).getErrorHi()
            Plotter.latex.DrawLatexNDC(.54, .8, r"#scale[0.8]{{+ {0:.3f} - {1:.3f} ps}}".format(tauErrorHi, tauErrorLo) )
        #if not frame =='B': self.DrawParams(pdfPlots)
        #%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%
        def chdir(path):
            os.makedirs(path, exist_ok=True) 
            os.chdir(path)
        if "plot_bkgCombA" in pltName:
            path=os.path.join(modulePath, self.process.work_dir, "SideBandBkg")
            chdir(path)
        if 'plot_sig' in pltName:
            path=os.path.join(modulePath, self.process.work_dir, "SignalFits")
            chdir(path)
        if 'plot_peak' in pltName:
            path=os.path.join(modulePath, self.process.work_dir, "PeakingFits")
            chdir(path)
        if 'plot_bkgComb' in pltName:
            path=os.path.join(modulePath, self.process.work_dir, "BkgComb")
            chdir(path)
        if 'plot_bkgPar' in pltName:
            path=os.path.join(modulePath, self.process.work_dir, "BkgPartial")
            chdir(path)
        if pltName.split('.')[0].strip('_Alt') in ["plot_bkgA_KStar", "plot_bkgM_KStar", "plot_bkgPeak3D"]:
            path=os.path.join(modulePath, self.process.work_dir, "KStarPlots")
            chdir(path)
        self.canvasPrint(pltName.replace('.', '_') + plotFuncs[frame]['tag'])
        Plotter.canvas.cd()
    os.chdir(cwd)

types.MethodType(plotSimpleBLK, Plotter)

def plotEfficiency(self, data_name, pdf_name, data_hist, label):
    binningL = ROOT.RooBinning(len(dataCollection.sigTBins) - 1, dataCollection.sigTBins)
    pltName = "effi"
    data = self.process.sourcemanager.get(data_name)
    pdf = self.process.sourcemanager.get(pdf_name)
    if data == None or pdf == None:
        self.logger.logWARNING("Skip plotEfficiency. pdf or data not found")
        return
    args = pdf.getParameters(data)
    if (not self.process.cfg['args'].NoImport): FitDBPlayer.initFromDB(self.process.dbplayer.odbfile, args)

    #%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%
    cwd=os.getcwd()
    path=os.path.join(modulePath, self.process.work_dir, "Efficiency")
    os.makedirs(path, exist_ok=True)
    os.chdir(path)
    #%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%

    #Cos_L Efficiency
    cloned_frameL = Plotter.frameL.emptyClone("cloned_frameL")
    plotlabel= 'sig'
    pdf.Print("v")
    data.plotOn(cloned_frameL, ROOT.RooFit.MarkerStyle(7)) #, ROOT.RooFit.Binning(binningL))
    if not self.process.cfg['args'].NoFit: pdf.plotOn(cloned_frameL, *plotterCfg_sigStyleNoFill, ROOT.RooFit.Normalization(1, ROOT.RooAbsReal.Raw), Range("dtRange"))
    cloned_frameL.GetYaxis().SetTitle("Efficiency")
    # cloned_frameL.SetMaximum(1.5 * cloned_frameL.GetMaximum()*(100 if self.process.cfg['args'].NoFit else 1))

    cloned_frameL.Draw() #DrawWithResidue(self, cloned_frameL)
    # Plotter.latexQ2(self.process.cfg['binKey'])
    if not self.process.cfg['args'].NoFit: Plotter.latex.DrawLatexNDC(.75, .89, "#chi^{{2}}/NDF={0:.2f}".format(cloned_frameL.chiSquare(pdf.getParameters(data).getSize())))
    # Plotter.latexCMSSim()
    # Plotter.latexCMSExtra()
    self.canvasPrint(pltName + label + "_time")
    os.chdir(cwd)
   
setattr(Plotter, 'plotEfficiency', plotEfficiency)

plotterCfg = {
    'name': "plotter",
    'switchPlots': [],
    'plots': {},
}
plotterCfg_dataStyle = ()
plotterCfg_mcStyle = ()
plotterCfg_allStyle = (ROOT.RooFit.LineColor(1),)
plotterCfg_sigStyleNoFill = (ROOT.RooFit.LineColor(4), ROOT.RooFit.LineWidth(2))
#plotterCfg_sigStyle = (ROOT.RooFit.LineColor(4), ROOT.RooFit.DrawOption("FL"), ROOT.RooFit.FillColor(4), ROOT.RooFit.FillStyle(3001), ROOT.RooFit.VLines()); 
plotterCfg_sigStyle = (ROOT.RooFit.LineColor(4), ROOT.RooFit.DrawOption("L"))
plotterCfg_bkgStyle = (ROOT.RooFit.LineColor(2), ROOT.RooFit.LineStyle(9))
plotterCfg_parStyle = (ROOT.RooFit.LineColor(3), ROOT.RooFit.LineStyle(9))
plotterCfg_bkgStyle_KStar = (ROOT.RooFit.LineColor(8), ROOT.RooFit.LineStyle(1), ROOT.RooFit.FillColor(8), ROOT.RooFit.DrawOption("FL"), ROOT.RooFit.FillStyle(3001))


def plotPostfitBLK(self, pltName, dataReader, pdfPlots, frames='BLK'):
    """Specification of plotSimpleBLK for post-fit plots with peaking background"""
    for pIdx, plt in enumerate(pdfPlots):
        pdfPlots[pIdx] = self.initPdfPlotCfg(plt)

    for regionName in ["SB"]:
        drawRegionName = {'SB': "bmass1,bmass2", 'innerSB': "innerLSB,innerUSB", 'outerSB': "outerLSB,outerUSB"}.get(regionName, regionName)
        dataPlots = [["{0}.{1}".format(dataReader, regionName), plotterCfg_styles['dataStyle'] + (ROOT.RooFit.CutRange(regionName),), "Data"], ]
        for pIdx, p in enumerate(dataPlots):
            dataPlots[pIdx] = self.initDataPlotCfg(p)

        nTotal = dataPlots[0][0].sumEntries("", "bmass1,bmass2")
        print(nTotal)
        if regionName in ['SB', 'innerSB', 'outerSB']:
            modified_pdfPlots = list()
            for pdfList in pdfPlots:
                modified_pdfPlots.append([
                        pdfPlots[0][0],
                        pdfList[1] + (ROOT.RooFit.Normalization(nTotal, ROOT.RooAbsReal.NumEvent), Range("bmass1,bmass2"), NormRange("bmass1,bmass2"), ProjectionRange("bmass1,bmass2"), Components(pdfList[0].GetName())),
                        pdfList[2],
                        pdfList[3]
                    ])
        plotFuncs = {
            'B': {'func': Plotter.plotFrameB_fine, 'tag': "_Bmass"},
            'L': {'func': Plotter.plotFrameL, 'tag': "_dt"},
            'K': {'func': Plotter.plotFrameK, 'tag': "_dtErr"},
        }

        cwd=os.getcwd()
        for frame in frames:
            NoFit= self.process.cfg['args'].NoFit
            plotFuncs[frame]['func'](dataPlots=dataPlots, pdfPlots=modified_pdfPlots, legend=False if NoFit else True, Plotpdf=(not NoFit), NoPull=self.process.cfg['args'].NoPull)
            Plotter.latexQ2(self.process.cfg['binKey'])
            
            ##################################
            path=os.path.join(modulePath, self.process.work_dir, "FinalFit")
            if not os.path.exists(path):
                os.mkdir(path)
            os.chdir(path)
            ##################################
            self.canvasPrint(pltName.replace('.', '_') + '_' + regionName + plotFuncs[frame]['tag'])
        os.chdir(cwd)
types.MethodType(plotPostfitBLK, Plotter)

def GetPlotterObject(self):
    Year=self.cfg['args'].Year
    AltRange=self.cfg['args'].AltRange
    binKey = self.cfg['binKey']
    TwoStep = self.cfg['args'].TwoStep
    plotterCfg['plots']['plot_sigEfficiency']= {
        'func': [plotEfficiency],
        'kwargs': {
            'data_name': 'Eff_sig_{0}'.format(Year),
            'pdf_name' : 'effi_sig.{}'.format(Year),
            'data_hist': 'Eff_sig_{0}'.format(Year),
            'label': '_sig'}
    }
    plotterCfg['plots']['plot_peakEfficiency']= {
        'func': [plotEfficiency],
        'kwargs': {
            'data_name': 'Eff_peak_{0}'.format(Year),
            'pdf_name' : 'effi_peak.{}'.format(Year),
            'data_hist': 'Eff_peak_{0}'.format(Year),
            'label': '_peak'}
    }
    plotterCfg['plots']['plot_effi']= {
        'func': [functools.partial(plotSimpleBLK, frames='LK')],
        'kwargs': {
            'pltName': "plot_effi.{0}".format(Year),
            'dataPlots': [['Eff_sig_{0}'.format(Year), plotterCfg_mcStyle, "Simulation"], ],
            'pdfPlots': [['effi_sig.{}'.format(Year), plotterCfg_sigStyle, None, None], ],
            'marks': {'marks': ['sim']}}
    }    
    plotterCfg['plots']['plot_sigM'] = {
        'func': [functools.partial(plotSimpleBLK, frames='B')],
        'kwargs': {
            'pltName': "plot_sigM.{}".format(Year),
            'dataPlots': [["sigMCReader.{}.Fit".format(Year), plotterCfg_mcStyle, "Simulation"], ],
            'pdfPlots': [["f_sigM.{}".format(Year), plotterCfg_sigStyle+(Range("BsfitRange"),), None, "Total fit"], ],
            'marks': {'marks': ['sim']}}
    }    
    plotterCfg['plots']['plot_sigT']= {
        'func': [functools.partial(plotSimpleBLK, frames='L')],
        'kwargs': {
            'pltName': "plot_sigT.{0}".format(Year),
            'dataPlots': [["sigMCReader.{0}.Fit".format(Year), plotterCfg_mcStyle, "Simulation"], ],
            'pdfPlots': [["f_sigT.{}".format(Year), plotterCfg_sigStyle+(Range("dtRange"),), {'sigTtau': 'sigTtau_RECO'}, None], ],
            'marks': {'marks': ['sim']}}
    }
    plotterCfg['plots']['plot_sigE']= {
        'func': [functools.partial(plotSimpleBLK, frames='K')],
        'kwargs': {
            'pltName': "plot_sigE.{0}".format(Year),
            'dataPlots': [["sigMCReader.{0}.Fit".format(Year), plotterCfg_mcStyle, "Simulation"], ],
            'pdfPlots': [["f_sigE.{}".format(Year), plotterCfg_sigStyle, None, None], ],
            'marks': {'marks': ['sim']}}
    }
    plotterCfg['plots']['plot_sig3D']= {
        'func': [functools.partial(plotSimpleBLK, frames='BLK')],
        'kwargs': {
            'pltName': "plot_sig3D.{0}".format(Year),
            'dataPlots': [["sigMCReader.{0}.Fit".format(Year), plotterCfg_mcStyle, "Simulation"], ],
            'pdfPlots': [["f_sig3D.{}".format(Year), plotterCfg_sigStyle, {'sigTtau': 'sigTtau_RECO'}, None], ],
            'marks': {'marks': ['sim']}}
    }
    plotterCfg['plots']['plot_peakM'] = {
        'func': [functools.partial(plotSimpleBLK, frames='B')],
        'kwargs': {
            'pltName': "plot_peakM.{}".format(Year),
            'dataPlots': [["peakMCReader.{}.Fit".format(Year), plotterCfg_mcStyle, "Simulation"], ],
            'pdfPlots': [["f_bkgPeakM.{}".format(Year), plotterCfg_sigStyle+(Range("BdfitRange"),), None, "Total fit"], ],
            'marks': {'marks': ['sim']}}
    }
    plotterCfg['plots']['plot_peakT']= {
        'func': [functools.partial(plotSimpleBLK, frames='L')],
        'kwargs': {
            'pltName': "plot_peakT.{0}".format(Year),
            'dataPlots': [["peakMCReader.{0}.Fit".format(Year), plotterCfg_mcStyle, "Simulation"], ],
            'pdfPlots': [["f_bkgPeakT.{}".format(Year), plotterCfg_sigStyle+(Range("dtRange"),), {'bkgPeakTtau': 'bkgPeakTtau_RECO'}, None], ],
            'marks': {'marks': ['sim']}}
    }
    plotterCfg['plots']['plot_peakE']= {
        'func': [functools.partial(plotSimpleBLK, frames='K')],
        'kwargs': {
            'pltName': "plot_peakE.{0}".format(Year),
            'dataPlots': [["peakMCReader.{0}.Fit".format(Year), plotterCfg_mcStyle, "Simulation"], ],
            'pdfPlots': [["f_bkgPeakE.{}".format(Year), plotterCfg_sigStyle+(Range("dtErrRange"),), None, None], ],
            'marks': {'marks': ['sim']}}
    }
    plotterCfg['plots']['plot_bkgCombT']= {
        'func': [functools.partial(plotSimpleBLK, frames='L')],
        'kwargs': {
            'pltName': "plot_bkgCombT.{0}".format(Year),
            'dataPlots': [["dataReader.{0}.USB".format(Year), plotterCfg_mcStyle, "Simulation"], ],
            'pdfPlots': [["f_bkgCombT.{}".format(Year), plotterCfg_sigStyle+(Range("dtRange"),), None, None], ],
            'marks': {'marks': ['data']}}
    }
    plotterCfg['plots']['plot_bkgCombE']= {
        'func': [functools.partial(plotSimpleBLK, frames='K')],
        'kwargs': {
            'pltName': "plot_bkgCombE.{0}".format(Year),
            'dataPlots': [["dataReader.{0}.USB".format(Year), plotterCfg_mcStyle, "Simulation"], ],
            'pdfPlots': [["f_bkgCombE.{}".format(Year), plotterCfg_sigStyle+(Range("dtErrRange"),), None, None], ],
            'marks': {'marks': ['data']}}
    }
    plotterCfg['plots']['plot_bkgParT']= {
        'func': [functools.partial(plotSimpleBLK, frames='L')],
        'kwargs': {
            'pltName': "plot_bkgParT.{}".format(Year),
            'dataPlots': [["dataReader.{}.LSB".format(Year), plotterCfg_mcStyle, "Simulation"], ],
            'pdfPlots': [["f_bkgT.{}".format(Year), plotterCfg_sigStyle+(RooFit.Range("dtRange"),), None, None],
                         # ["f_bkgT.{}".format(Year), plotterCfg_bkgStyle+(RooFit.Range("dtRange"), RooFit.Components('f_bkgCombT'),), None, None],
                         # ["f_bkgT.{}".format(Year), plotterCfg_parStyle+(RooFit.Range("dtRange"), RooFit.Components('f_bkgParT')), None, None],
                        ],
            'marks': {'marks': ['data']}}
    }
    plotterCfg['plots']['plot_bkgParE']= {
        'func': [functools.partial(plotSimpleBLK, frames='K')],
        'kwargs': {
            'pltName': "plot_bkgParE.{}".format(Year),
            'dataPlots': [["dataReader.{}.LSB".format(Year), plotterCfg_mcStyle, "Simulation"], ],
            'pdfPlots': [["f_bkgE.{}".format(Year), plotterCfg_sigStyle+(RooFit.Range("dtErrRange"),), None, None],
                         # ["f_bkgE.{}".format(Year), plotterCfg_bkgStyle+(RooFit.Range("dtErrRange"), RooFit.Components('f_bkgCombE'),), None, None],
                         # ["f_bkgE.{}".format(Year), plotterCfg_parStyle+(RooFit.Range("dtErrRange"), RooFit.Components('f_bkgParE')), None, None],
                        ],
            'marks': {'marks': ['data']}}
    }
    plotterCfg['plots']['plot_bkg2D']= { # NOT USED
        'func': [functools.partial(plotSimpleBLK, frames='LK')],
        'kwargs': {
            'pltName': "plot_bkg2D.{}".format(Year),
            'dataPlots': [["dataReader.{}.SB".format(Year), plotterCfg_mcStyle, "Simulation"], ],
            'pdfPlots': [["f_bkg2D.{}".format(Year), plotterCfg_sigStyle+(RooFit.Range("dtRange,bmass1,bmass2"),), None, None],
                         ["f_bkg2D.{}".format(Year), plotterCfg_bkgStyle+(RooFit.Range("dtRange"), RooFit.Components('f_bkgCombE'),), None, None],
                         ["f_bkg2D.{}".format(Year), plotterCfg_parStyle+(RooFit.Range("dtRange"), RooFit.Components('f_bkgParE')), None, None],
                        ],
            'marks': {'marks': ['data']}}
    }
    plotterCfg['plots']['plot_bkg3D'] = {
    'func': [functools.partial(plotPostfitBLK, frames='B')],
    'kwargs': {
        'pltName': "plot_bkg3D.{}".format(Year),
        'dataReader': "dataReader.{}".format(Year),
        'pdfPlots': [["f_bkg3D.{}".format(Year), plotterCfg_styles['allStyleBase'], None, "Total fit"],
                     ["f_bkgCombM.{}".format(Year), plotterCfg_styles['bkgStyleBase'], None, "Combinatorial Bkg"],
                     ["f_bkgParM.{}".format(Year), plotterCfg_parStyle, None, "Semileptonic Bkg"],],
        }
    }

    plotter=Plotter(plotterCfg)
    if self.cfg['args'].SimFit and (self.cfg['args'].seqKey=='fitSig2D'):
        plotter.cfg['switchPlots'].append('Combined_plot_sig2D')
    elif self.cfg['args'].SimFit and self.cfg['args'].seqKey=='fitSigMCGEN':
        plotter.cfg['switchPlots'].append('Combined_plot_sigMCGEN')
    else:
        for plot in self.cfg['args'].list: plotter.cfg['switchPlots'].append(plot)
    return plotter

if __name__ == '__main__':
    binKey =  [key for key in tbins.keys() if tbins[key]['label']==sys.argv[1]]
    for b in binKey:
        p.cfg['binKey'] = b
        plotter.cfg['switchPlots'].append('simpleSpectrum')                # Bmass 1D Fit
        dataCollection.effiHistReader=dataCollection.effiHistReaderOneStep
        p.setSequence([dataCollection.effiHistReader, dataCollection.sigMCReader, dataCollection.sigMCGENReader, dataCollection.dataReader, pdfCollection.stdWspaceReader, plotter])
        p.beginSeq()
        p.runSeq()
        p.endSeq()
