#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set sw=4 ts=4 fdm=indent fdl=2 ft=python et:

import os, re, itertools, pdb, ctypes
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from v2Fitter.Fitter.FitterCore import FitterCore

import LifetimeFitter.cpp
from LifetimeFitter.anaSetup import tbins, modulePath
from LifetimeFitter.StdProcess import setStyle
from LifetimeFitter.varCollection import treco
from LifetimeFitter.FitDBPlayer import FitDBPlayer

class EfficiencyFitter(FitterCore):
    """Implementation to standard efficiency fitting procdeure to BuToKstarMuMu angular analysis"""

    @classmethod
    def templateConfig(cls):
        cfg = FitterCore.templateConfig()
        cfg.update({
            'name': "EfficiencyFitter",
            'datahist': "effiHistReader.h_eff",
            'data': "effiHistReader.accXrec",
            'dataX': "effiHistReader.h_accXrec_fine_ProjectionX",
            'dataY': "effiHistReader.h_accXrec_fine_ProjectionY",
            'pdf': "effi_sigA",
            'pdfX': "effi_cosl",
            'pdfY': "effi_cosK",
            'updateArgs': True,
        })
        del cfg['createNLLOpt']
        return cfg

    def _bookMinimizer(self):
        """Pass complicate fitting control."""
        pass

    def _preFitSteps(self):
        """Prefit uncorrelated term"""
        if not self.process.cfg['args'].NoImport: 
            FitDBPlayer.initFromDB(self.process.dbplayer.odbfile, self.pdf.getParameters(self.data))

    def _postFitSteps(self):
        """Post-processing"""
        args = self.pdf.getParameters(self.data)
        self.ToggleConstVar(args, True)
        FitDBPlayer.UpdateToDB(self.process.dbplayer.odbfile, args)

    def _runFitSteps(self):
        """Main fitter step"""
        h_eff = self.process.sourcemanager.get(self.cfg['datahist']) 
        args = self.pdf.getParameters(self.data)
        nPar = 0
        floaters = {} #Saving indices floating parameters
        def GetTFunction():
            """Convert RooFormulaVar to TF1"""
            nonlocal nPar
            args_it = args.createIterator()
            arg = args_it.Next()
            effi_formula = self.pdf.formula().formulaString()
            paramDict ={}
            for p in range(self.pdf.formula().actualDependents().getSize()):
                paramDict[self.pdf.formula().getParameter(p).GetName()]=p
            effi_formula = effi_formula.replace("x[{0}]".format(paramDict['treco']), "x")
            while arg:
                effi_formula = effi_formula.replace('x[{0}]'.format(paramDict[arg.GetName()]), "[{0}]".format(nPar))
                floaters[nPar] = arg.GetName()
                nPar = nPar + 1
                arg = args_it.Next()
            f_effi = ROOT.TF1("f_effi", effi_formula, 0.2, 10)
            return f_effi

        # Using TMinuit. PDF in terms of RooFormulaVar. 
        # Does not work for RooProdPdf or RooProduct
        f_effi = GetTFunction()
        FitterCore.ArgLooper(args, lambda p: p.Print())
        for idx, name in floaters.items():
            f_effi.SetParameter(idx, args.find(name).getVal())
            # f_effi.SetParLimits(idx, args.find(name).getMax(), args.find(name).getMin())
        h_eff.Fit("f_effi", "RM")
        h_eff.Fit("f_effi", "RM")

        #Update parameter values
        for idx, name in floaters.items():
            args.find(name).setVal(f_effi.GetParameter(idx))
            args.find(name).setError(f_effi.GetParError(idx))

        # Plot comparison between fitting result to data
        setStyle()
        FitterCore.ArgLooper(args, lambda p: p.Print())

    @staticmethod
    def isPosiDef(formula2D):
        f2_min_x, f2_min_y = ctypes.c_double(0.), ctypes.c_double(0.)
        formula2D.GetMinimumXY(f2_min_x, f2_min_y)
        f2_min = formula2D.Eval(f2_min_x, f2_min_y)
        if f2_min > 0:
            return True
        else:
            print("WARNING\t: Sanitary check failed: Minimum of efficiency map is {0:.2e} at {1}, {2}".format(f2_min, f2_min_x, f2_min_y))
        return False

