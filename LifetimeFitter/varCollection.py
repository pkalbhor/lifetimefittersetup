#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set sw=4 ts=4 fdm=indent fdl=2 ft=python et:

# Description     : Shared object definition.

from ROOT import RooRealVar
from ROOT import RooArgSet

Bmass       = RooRealVar("Bmass","m_{J/#psi K_{S}^{0}}", 5.1, 5.6, "GeV")
treco       = RooRealVar("treco", "Decay time", 0.0, 10., "ps")
trecoe      = RooRealVar("trecoe", "Decay time error", 0.01, 0.15, "ps")
Bmass.setRange("BsfitRange", 5.26, 5.46)
Bmass.setRange("BdfitRange", 5.17, 5.37)
treco.setRange("dtRange", 0.2, 10.)
trecoe.setRange("dtErrRange", 0.01, 0.15)

Bmass.setRange("bmass1", 5.1, 5.17)
treco.setRange("bmass1", 0.2, 10.)
trecoe.setRange("bmass1", 0.01, 0.15)
Bmass.setRange("bmass2", 5.45, 6.0)
treco.setRange("bmass2", 0.2, 10.)
trecoe.setRange("bmass2", 0.01, 0.15)
Bdt        	= RooRealVar("Bdt", "Bdt", 0.05, 1.)
is_truebs  	= RooRealVar("is_truebs", "is_truebs", 0, 1)
Puw8        = RooRealVar("Puw8", "Pile up weight", 0, 5)
Reweight	= RooRealVar("Reweight", "Reweight", 0, 5)

#ArgSet for Data
dataArgs = RooArgSet(Bmass, treco, trecoe, Bdt)

#ArgSet for MC
MCArgSet = RooArgSet(Bmass, treco, trecoe, Bdt, is_truebs, Puw8)

#ArgSet for Peaking Bkg
MCPeakArgSet   = RooArgSet(Bmass, treco, trecoe, Bdt, Puw8)

MCGenArgSet = RooArgSet()
