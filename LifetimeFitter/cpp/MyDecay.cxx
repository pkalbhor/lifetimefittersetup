#include "MyDecay.h"
 
#include "RooFit.h"
#include "RooRealVar.h"
#include "RooRandom.h"
 
#include "TError.h"
 
using namespace std;
 
ClassImp(MyDecay);
 
////////////////////////////////////////////////////////////////////////////////
/// Create a new MyDecay.
/// \param[in] name Name of this object.
/// \param[in] title Title (for *e.g.* plotting)
/// \param[in] t Convolution variable (*e.g.* time).
/// \param[in] tau Decay constant.
/// \param[in] model Resolution model for the convolution.
/// \param[in] type One of the decays types `SingleSided, Flipped, DoubleSided`
MyDecay::MyDecay(const char *name, const char *title,
         RooRealVar& t, RooAbsReal& tau,
         const RooResolutionModel& model, DecayType type) :
  RooAbsAnaConvPdf(name,title,model,t),
  _t("t","time",this,t),
  _tau("tau","decay time",this,tau),
  _type(type)
{
  switch(type) {
  case SingleSided:
    _basisExp = declareBasis("exp(-@0/@1)",tau) ;
    break ;
  case Flipped:
    _basisExp = declareBasis("exp(@0/@1)",tau) ;
    break ;
  case DoubleSided:
    _basisExp = declareBasis("exp(-abs(@0)/@1)",tau) ;
    break ;
  }
}
 
////////////////////////////////////////////////////////////////////////////////
/// Create a new MyDecay: 2nd Type.
/// \param[in] name Name of this object.
/// \param[in] title Title (for *e.g.* plotting)
/// \param[in] t Convolution variable (*e.g.* time).
/// \param[in] tau Decay constant.
/// \param[in] model Resolution model for the convolution.
/// \param[in] type One of the decays types `SingleSided, Flipped, DoubleSided`
MyDecay::MyDecay(const char *name, const char *title,
         RooRealVar& t, RooAbsReal& tau1, RooAbsReal& tau2, RooAbsReal& param1,
         const RooResolutionModel& model, DecayType type) :
  RooAbsAnaConvPdf(name,title,model,t),
  _t("t","time",this,t),
  _tau1("tau2","decay time",this,tau1),
  _tau2("tau2","decay time",this,tau2),
  _param1("param1","param1",this,param1),
  _type(type)
{
  switch(type) {
  case SingleSided:
    _basisExp = declareBasis("(@3*exp(-@0/@1)+(1-@3)*exp(-@0/@2))", RooArgList(tau1, tau2, param1)) ;
    break ;
  case Flipped:
    _basisExp = declareBasis("@0*(@3*exp(@0/@1)+(1-@3)*exp(@0/@2))", RooArgList(tau1, tau2, param1)) ;
    break ;
  case DoubleSided:
    _basisExp = declareBasis("abs(@0)*(@3*exp(-abs(@0)/@1)+(1-@3)*exp(-abs(@0)/@2))", RooArgList(tau1, tau2, param1)) ;
    break ;
  }
}
 
////////////////////////////////////////////////////////////////////////////////
/// Copy constructor
 
MyDecay::MyDecay(const MyDecay& other, const char* name) :
  RooAbsAnaConvPdf(other,name),
  _t("t",this,other._t),
  _tau("tau",this,other._tau),
  _type(other._type),
  _basisExp(other._basisExp)
{
}
 
////////////////////////////////////////////////////////////////////////////////
/// Destructor
 
MyDecay::~MyDecay()
{
}
 
////////////////////////////////////////////////////////////////////////////////
 
Double_t MyDecay::coefficient(Int_t /*basisIndex*/) const
{
  return 1 ;
}
 
////////////////////////////////////////////////////////////////////////////////
 
Int_t MyDecay::getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t /*staticInitOK*/) const
{
  if (matchArgs(directVars,generateVars,_t)) return 1 ;
  return 0 ;
}
 
////////////////////////////////////////////////////////////////////////////////
 
void MyDecay::generateEvent(Int_t code)
{
  R__ASSERT(code==1) ;
 
  // Generate delta-t dependent
  while(1) {
    Double_t rand = RooRandom::uniform() ;
    Double_t tval(0) ;
 
    switch(_type) {
    case SingleSided:
      tval = -_tau*log(rand);
      break ;
    case Flipped:
      tval= +_tau*log(rand);
      break ;
    case DoubleSided:
      tval = (rand<=0.5) ? -_tau*log(2*rand) : +_tau*log(2*(rand-0.5)) ;
      break ;
    }
 
    if (tval<_t.max() && tval>_t.min()) {
      _t = tval ;
      break ;
    }
  }
}