#ifndef MY_DECAY
#define MY_DECAY
 
#include "RooAbsAnaConvPdf.h"
#include "RooRealProxy.h"
 
class MyDecay : public RooAbsAnaConvPdf {
public:
 
  enum DecayType { SingleSided, DoubleSided, Flipped };
 
  // Constructors, assignment etc
  inline MyDecay() { }
  MyDecay(const char *name, const char *title, RooRealVar& t,
      RooAbsReal& tau, const RooResolutionModel& model, DecayType type) ;
  MyDecay(const char *name, const char *title, RooRealVar& t,
      RooAbsReal& tau1, RooAbsReal& tau2, RooAbsReal& param1, const RooResolutionModel& model, DecayType type) ;
  MyDecay(const MyDecay& other, const char* name=0);
  virtual TObject* clone(const char* newname) const { return new MyDecay(*this,newname) ; }
  virtual ~MyDecay();
 
  virtual Double_t coefficient(Int_t basisIndex) const ;
 
  Int_t getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t staticInitOK=kTRUE) const;
  void generateEvent(Int_t code);
 
protected:
 
  RooRealProxy _t ;
  RooRealProxy _tau ;
  RooRealProxy _tau1 ;
  RooRealProxy _tau2 ;
  RooRealProxy _param1 ;
  DecayType    _type ;
  Int_t        _basisExp ;
 
  ClassDef(MyDecay,1) // General decay function p.d.f
};
 
#endif