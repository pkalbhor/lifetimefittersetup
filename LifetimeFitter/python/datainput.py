# Description: Input data files
# 
# Author     : Pritam Kalbhor (physics.pritam@gmail.com)
#
#
##################################################################################################

import os
import subprocess
#GITDIR=subprocess.check_output("git rev-parse --show-toplevel", shell=True).strip()

genSel          = "1"
recBaseSel      = "1"
ExtraCuts       = "1"

def GetInputFiles(self):
    Args=self.cfg['args']
    path0="/eos/user/p/pkalbhor/BPData/JpsiKsLifetime/"

    self.cfg['allBins']      = ["full"]
    if Args.Year==2016:
        self.cfg['dataFilePath'] = [path0+"Data_2016.root/tree"]
        self.cfg['sigMC']        = [path0+"BsMC_2016.root/tree"] 
        self.cfg['peaking'] = {'BdMC': [path0+"BdMC_2016.root/tree"]}
        self.cfg['control'] = {'BdMC': [path0+"BdMC_2016.root/tree"]}
    if Args.Year==2017:
        self.cfg['dataFilePath'] = [path0+"Data_2017.root/tree"]
        self.cfg['sigMC']        = [path0+"BsMC_2017.root/tree"] 
        self.cfg['peaking'] = {'BdMC': [path0+"BdMC_2017.root/tree"]}
        self.cfg['control'] = {'BdMC': [path0+"BdMC_2017.root/tree"]}

    if Args.Year==2018:
        self.cfg['dataFilePath'] = [path0+"Data_2018.root/tree"]
        self.cfg['sigMC']        = [path0+"BsMC_2018.root/tree"] 
        self.cfg['peaking'] = {'BdMC': [path0+"BdMC_2018.root/tree"]}
        self.cfg['control'] = {'BdMC': [path0+"BdMC_2018.root/tree"]}

    # Cut strings

    if Args.Year==2016:
        cut_Bdt = "Bdt > 0.64"
    if Args.Year==2017:
        cut_Bdt = "Bdt > 0.63"
    if Args.Year==2018:
        cut_Bdt = "Bdt > 0.58"
    cuts = [
        cut_Bdt,
    ]
    cuts.append("({0})".format(")&&(".join(cuts)))
    self.cfg['cuts'] = cuts
