from LifetimeFitter.python.ArgParser import SetParser, GetBatchTaskParser
parser=GetBatchTaskParser()        
args = parser.parse_known_args()[0]
from   LifetimeFitter.StdProcess import p
p.work_dir="plots_{}".format(args.Year)
p.cfg['args'] = args           

from LifetimeFitter.plotCollection import GetPlotterObject
import LifetimeFitter.seqCollection as seqCollection
from LifetimeFitter.anaSetup import tbins, modulePath
import os, re

seqKey  = list(seqCollection.predefined_sequence.keys())
binKeys = [tbins[k]['label'] for k in tbins.keys()]
plotList= list(GetPlotterObject(p).cfg['plots'].keys())

dirlist = re.findall(r"\w*.py\b",  " ".join(os.listdir(modulePath)))

print(dirlist)
print(binKeys)
print(seqKey)
print(plotList)
List=" ".join(binKeys+seqKey+plotList)
os.system("""complete -W \"{0}\" seqCollection.py """.format(List))
