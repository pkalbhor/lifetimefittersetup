# Description : Final fitter using minimum package modules
#
# Author      : Pritam Kalbhor (physics.pritam@gmail.com)
#
#-----------------------------------------------------------------------

import os, sys, pdb, shelve, ROOT
from ROOT import RooRealVar, RooFit
from LifetimeFitter.anaSetup import modulePath
from v2Fitter.Fitter.FitterCore import FitterCore
from LifetimeFitter.FitDBPlayer import FitDBPlayer

Bmass       = RooRealVar("Bmass","m_{J/#psi K_{S}^{0}}", 5.1, 5.6, "GeV")
treco       = RooRealVar("treco", "Decay time", 0.0, 10., "ps")                                                                          
trecoe      = RooRealVar("trecoe", "Decay time error", 0.01, 0.2, "ps")
Bdt         = RooRealVar("Bdt", "Bdt", 0.05, 1.)
is_truebs   = RooRealVar("is_truebs", "is_truebs", 0, 1)
Puw8        = RooRealVar("Puw8", "Pile up weight", 0, 5)

ArgSet = ROOT.RooArgSet(Bmass, treco, trecoe, Bdt, is_truebs, Puw8)
#----------------------------
# Create DataSet and PDFs
#----------------------------
sigFile = ROOT.TFile("/eos/user/p/pkalbhor/BPData/JpsiKsLifetime/BsMC_2017.root")
sigTree = sigFile.Get('tree')

RooCut = ROOT.RooFit.Cut("Bdt > 0.63 && is_truebs==1")
Weight = ROOT.RooFit.WeightVar("Puw8")
sigData = ROOT.RooDataSet("FitDataset", "Weighted dataset", ArgSet, ROOT.RooFit.Import(sigTree), RooCut, Weight)

# Retrieve efficiency function
wFile = ROOT.TFile(modulePath+"/input/wspace_2017_full.root")
wspace = wFile.Get("wspace.2017.full")
effi_sig = wspace.obj("effi_sig")

# Initialize efficiency parameters
dbfile = modulePath+"/plots_2017/fitResults_full.db"
args = effi_sig.getParameters(ROOT.RooArgSet(treco, trecoe))
FitDBPlayer.initFromDB(dbfile, args)

tau_Sig = RooRealVar("tau_Sig", "tau_Sig", 1.472, .0, 6.)
bias    = RooRealVar("bias", "bias", 0)#, 0, 1)
sigma   = RooRealVar("sigma", "sigma", 1) #, 0, 3)
f_sigResolution = ROOT.RooGaussModel("f_sigResolution", "f_sigResolution", treco, bias, sigma, trecoe)
# f_sigResolution = ROOT.RooGaussModel("f_sigResolution", "f_sigResolution", treco, bias, sigma)
# f_sigResolution = ROOT.RooTruthModel("f_sigResolution", "f_sigResolution", treco)
f_sigD = ROOT.RooDecay("f_sigD", "f_sigD", treco, tau_Sig, f_sigResolution, ROOT.RooDecay.SingleSided)
f_sigD.Print("v")
f_sigT = ROOT.RooEffProd("f_sigT", "f_sigT", f_sigD, effi_sig)

f_sigT.Print("v")
args.Print("v")

#----------------------------
# Assemble TMinuit Minimizer
#----------------------------
# bias.setConstant()
# sigma.setConstant()

treco.setRange("tRange", 0.2, 10)
# trecoe.setRange("ErrRange", 0.02, 0.15)
#  ROOT.RooFit.Range("tRange,ErrRange"),

result = f_sigT.fitTo(sigData, RooFit.Save(), ROOT.RooFit.Range("tRange"), ROOT.RooFit.ConditionalObservables(ROOT.RooArgSet(trecoe)))

#----------------------------
# Plot Post-Fit Distributions
#----------------------------
c1 = ROOT.TCanvas("c1");
frame = treco.frame()
sigData.plotOn(frame)
f_sigT.plotOn(frame)
frame.Draw()

c2 = ROOT.TCanvas("c2");
effFile = ROOT.TFile(modulePath+"/data/EfficiencyHists_2017_full.root")
effHist = effFile.Get("Hist_sig_2017_full")
rooHist = ROOT.RooDataHist("rooHist", "", ROOT.RooArgList(treco), ROOT.RooFit.Import(effHist))
frame2 = frame.emptyClone("name")
rooHist.plotOn(frame2)
effi_sig.plotOn(frame2)
frame2.Draw()
