import ROOT, pdb
from LifetimeFitter.python.ArgParser import SetParser, GetBatchTaskParser
parser=GetBatchTaskParser()
args = parser.parse_known_args()[0]


from LifetimeFitter.StdProcess import p, setStyle
setStyle()
ROOT.gStyle.SetPadRightMargin(0.15)

p.work_dir="plots_"+str(args.Year)
p.cfg['args'] = args
from LifetimeFitter.python.datainput import GetInputFiles, ExtraCuts
GetInputFiles(p)

from LifetimeFitter.varCollection import MCArgSet, treco, trecoe
# wfile = ROOT.TFile("input/wspace_2016_full.root", "READ")
# wspace = wfile.Get("wspace.2016.full")

myspace = ROOT.RooWorkspace("myspace")
myspace.Import(treco)
myspace.Import(trecoe)
factoryCMD = [ 
		"k0[4.36627e-04, 0,1.]", "k2[-7.97688e-02, -40, 40]",
		"k1[-5.66842e+00, -30,30]", "k3[7.29125e+01, -600, 600]", "k4[2.07728e+00, -100,100]",
		"expr::effi_sig(' TMath::Max(1e-6, k0*(1+k1*treco+k2*pow(treco,2)+k3/(1+exp(-treco*k4))) ) ', {treco, k0, k1, k2, k3, k4})",
		"RooGaussModel::f_sigResolution(treco, bias[0, 0, 1], sigma[1, 0, 3], trecoe)",
        "RooDecay::f_sigD(treco, tau_Sig[1.472, 0.01, 10.], f_sigResolution, RooDecay::DoubleSided)",
        "RooEffProd::f_sigT(f_sigD, effi_sig)",
]
for command in factoryCMD:
	myspace.factory(command)
pdf = myspace.obj("f_sigT")
dfile = ROOT.TFile("data/preload_sigMCReader_2016_full.root", "READ")
data = dfile.Get("sigMCReader.2016.Fit")

from v2Fitter.Fitter.FitterCore import FitterCore
args = pdf.getParameters(data)
pdb.set_trace()

FitterCore.ToggleConstVar(args, True, ['k0', 'k1', 'k2', 'k3', 'k4'])
FitterCore.ArgLooper(args, lambda var: print(var.GetName(), var.isConstant()))
FitterCore.ArgLooper(args, lambda var: var.Print())
from LifetimeFitter.FitDBPlayer import FitDBPlayer
FitDBPlayer.initFromDB("plots_2016/fitResults_full.db", args, None, ["bias", "sigma", "tau_Sig"])
FitterCore.ArgLooper(args, lambda var: var.Print())
pdf.fitTo(data, ROOT.RooFit.SumW2Error(True), ROOT.RooFit.Range(0.2, 10), ROOT.RooFit.ConditionalObservables(ROOT.RooArgSet(trecoe)))


frame = treco.frame()
data.plotOn(frame)
pdf.plotOn(frame, ROOT.RooFit.Range(0.2, 10))
frame.Draw()
ROOT.gPad.SaveAs("sigt.pdf")