# Description: Plot variables from a file
#
#
#
##################################################################################################

import os, pdb, ROOT
from LifetimeFitter.seqCollection import args as Args
from LifetimeFitter.StdProcess import p
from LifetimeFitter.python.datainput import GetInputFiles, ExtraCuts, ExtraCutsKStar, genSel
p.cfg['args'] = Args
GetInputFiles(p)

from LifetimeFitter.anaSetup import tbins, bMassRegions
ROOT.gStyle.SetOptTitle(0)               # turn off the title
ROOT.gROOT.SetBatch(True)

def Decorate(hist, name):
    hist.GetXaxis().SetName(name)
f=ROOT.TChain()
files=[]
Var=['Bmass']
if Args.seqKey=='KStar':
    files = p.cfg['KStarSigMC']
    Var=['Kst0mass'] #, 'Phimass', 'CosThetaK', 'CosThetaL', 'Bmass']
if Args.seqKey=='genPhiMuMu':
    files = p.cfg['genonly']['PhiMuMu']
    Var=['genBpid', 'genBPhi', 'genMupPt', 'genMupEta', 'genMupPhi', 'genMumPt', 'genMumEta', 'genMumPhi', 'gendimuPt', 'genPhipt', 'gendimuEta', 'gendimuPhi', 'genPhieta', 'genPhiphi', 'genKpPt', 'genKpEta', 'genKmPt', 'genKmEta', 'genQ2', 'genCosThetaL', 'genCosThetaK']
if Args.seqKey=='genOff_PhiMuMu':
    files = p.cfg['genOff']['PhiMuMu']
    Var=['genBpid', 'genBPhi', 'genMupPt', 'genMupEta', 'genMupPhi', 'genMumPt', 'genMumEta', 'genMumPhi', 'gendimuPt', 'genPhipt', 'gendimuEta', 'gendimuPhi', 'genPhieta', 'genPhiphi', 'genKpPt', 'genKpEta', 'genKmPt', 'genKmEta', 'genQ2', 'genCosThetaL', 'genCosThetaK']
if Args.seqKey=='Data':
    files = p.cfg['dataFilePath']
    Var=['Bmass']
for ch in files:
    f.Add(ch)
c1=ROOT.TCanvas("c1", "c1"); c1.cd()

XXTra = "(1)"
if False: XXTra = '!(CosThetaL >-.5 && CosThetaL <.5)'
tcut    =" && ".join([p.cfg['cuts'][-1], ExtraCutsKStar if Args.seqKey=='KStar' else ExtraCuts, XXTra])
for var in Var:
    for binKey in ['summary']: #tbins.keys():
        #if not binKey in ['belowJpsiB']: continue
        #finalcut=" && ".join([tbins[binKey]['cutString'], tcut, bMassRegions['Fit']['cutString']])
        finalcut=tbins[binKey]['cutString'] if not Args.seqKey in ['genPhiMuMu', 'genOff_PhiMuMu'] else tbins[binKey]['cutString'].replace("Q2", "genQ2") + "&&" +genSel
        print(finalcut)
        f.Draw(var, finalcut)#, 'E1')
        hist=ROOT.gPad.GetPrimitive("htemp"); 
        if type(hist) is not ROOT.TObject: 
            hist.SetName(var)
            #hist.Rebin(5)
        ROOT.TLatex().DrawLatexNDC(.45, .89, r"#scale[0.8]{{{latexLabel}}}".format(latexLabel=tbins[binKey]['latexLabel']))
        ROOT.TLatex().DrawLatexNDC(.45, .84, r"#scale[0.8]{{Events = {0:.2f}}}".format(f.GetEntries(finalcut)) )
        c1.SaveAs("python/{2}_{0}_{1}_{binKey}.pdf".format(var, Args.Year, Args.seqKey, binKey=tbins[binKey]['label']))
os.system("root -l")
