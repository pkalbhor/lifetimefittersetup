#!/usr/bin/python3 -u
# -*- coding: utf-8 -*-
# vim: set sw=4 ts=4 fdm=indent fdl=1 fdn=3 ft=python et:

import sys, os, pdb, datetime, importlib
import ROOT
#ROOT.EnableImplicitMT()

#Supress RooFit related info messages
ROOT.gEnv.SetValue("RooFit.Banner", 0)
ROOT.RooMsgService.instance().setGlobalKillBelow(3)

#Verify if PYTHONPATH is set
if importlib.util.find_spec("LifetimeFitter") is None:
    raise ModuleNotFoundError("Please source setup_ROOTEnv.sh script and come back again!")

import v2Fitter.FlowControl.Logger as Logger
log = Logger.Logger("runtime_temp.log", 999) #Logger.VerbosityLevels.DEBUG)

from LifetimeFitter.python.ArgParser import SetParser, GetBatchTaskParser
parser=GetBatchTaskParser()
args = parser.parse_known_args()[0]

# Standard sequences; To be pre-defined
predefined_sequence = {}
predefined_sequence['loadData']  = ['dataReader']
predefined_sequence['loadMC']    = ['sigMCReader']
predefined_sequence['loadMCPeak']= ['peakMCReader']
predefined_sequence['buildPdfs'] = ['stdWspaceReader', 'stdPDFBuilder']
predefined_sequence['buildEff']  = ['effiHistReader']

predefined_sequence['fitEff']    = ['effiHistReader', 'stdWspaceReader', 'sigEffiFitter', 'peakEffiFitter']
predefined_sequence['fitEff2']   = ['effiHistReader', 'stdWspaceReader', 'effiFitter2']

predefined_sequence['fitSigM']   = ['sigMCReader', 'stdWspaceReader', 'sigMFitter']
predefined_sequence['fitSigT']   = ['sigMCReader', 'stdWspaceReader', 'sigTFitter']
predefined_sequence['fitSigE']   = ['sigMCReader', 'stdWspaceReader', 'sigEFitter']
predefined_sequence['fitSig3D']  = ['sigMCReader', 'stdWspaceReader', 'sig3DFitter']

predefined_sequence['fitPeakM']   = ['peakMCReader', 'stdWspaceReader', 'peakMFitter']
predefined_sequence['fitPeakT']   = ['peakMCReader', 'stdWspaceReader', 'peakTFitter']
predefined_sequence['fitPeakE']   = ['peakMCReader', 'stdWspaceReader', 'peakEFitter']
predefined_sequence['fitPeak3D']  = ['peakMCReader', 'stdWspaceReader', 'peak3DFitter']

predefined_sequence['fitBkgCombT']= ['dataReader', 'stdWspaceReader', 'bkgCombTFitter']
predefined_sequence['fitBkgCombE']= ['dataReader', 'stdWspaceReader', 'bkgCombEFitter']

predefined_sequence['fitBkgParT'] = ['dataReader', 'stdWspaceReader', 'bkgParTFitter']
predefined_sequence['fitBkgParE'] = ['dataReader', 'stdWspaceReader', 'bkgParEFitter']
predefined_sequence['fitBkg2D']= ['dataReader', 'stdWspaceReader', 'bkg2DFitter']
predefined_sequence['fitBkg3D']= ['dataReader', 'stdWspaceReader', 'bkg3DFitter']

predefined_sequence['fitSigMCGEN']=(['sigMCGENReader', 'stdWspaceReader'], ['SimulFitter_sigGEN'] if args.SimFit else ['sigAFitter'])
predefined_sequence['fitBkgPeak3D'] = ['KsigMCReader', 'stdWspaceReader', 'bkgPeak3DFitter']

predefined_sequence['fitFinalM']  = ['dataReader', 'stdWspaceReader', 'finalMFitter'] #1D Bmass Fit
predefined_sequence['fitFinal3D'] = ['dataReader', 'stdWspaceReader', 'final3DFitter'] #3D Bmass Fit

predefined_sequence['createplots']=['effiHistReader', 
                                    'sigMCReader', 'peakMCReader',
                                    'dataReader', 
                                    'stdWspaceReader', 'plotter']
predefined_sequence['mixedToyValidation'] = (['stdWspaceReader', 'sigMCReader', 'bkgCombToyGenerator', 'bkgPeakToyGenerator'], ['mixedToyStudier']) if not args.Toy2 else (['stdWspaceReader', 'sigMCReader'], ['mixedToyStudier'])
predefined_sequence['FinalDataResult'] = ['FinalDataResult']
predefined_sequence['EffiTable']       = ['EffiTable']
predefined_sequence['StatusTable']  = ['StatusTableMaker']

def Instantiate(self, seq):
    """All objects from 'predefined_sequence' are initalized here"""
    # Classes in use are: DataReader, WSpaceReader, StdFitter, ObjectProvider, EfficiencyFitter, Plotter
    import LifetimeFitter.dataCollection as dataCollection
    import LifetimeFitter.pdfCollection  as pdfCollection
    import LifetimeFitter.fitCollection  as fitCollection
    from LifetimeFitter.plotCollection import GetPlotterObject
    sequence=[]
    dataSequence=['sigMCReader', 'dataReader', 'peakMCReader', 'StatusTableMaker']
    fitSequence=['sigEffiFitter', 'peakEffiFitter', 
                 'sigMFitter', 'sigTFitter', 'sigEFitter', 'sig3DFitter', 
                 'peakMFitter', 'peakTFitter', 'peakEFitter', 'peak3DFitter', 'bkgCombTFitter',
                 'bkgCombEFitter', 'bkgParTFitter', 'bkgParEFitter', 'bkg2DFitter',
                 'bkg3DFitter', 'final3DFitter',
                ]
    for s in seq:
        if s in dataSequence:
            sequence.append(dataCollection.GetDataReader(self, s))
        if s is 'stdWspaceReader':
            sequence.append(pdfCollection.GetWspaceReader(self))
        if s is 'stdPDFBuilder':
            sequence.append(pdfCollection.stdPDFBuilder)
        if s is 'effiHistReader':
            sequence.append(dataCollection.effiHistReaderOneStep) if not args.TwoStep else sequence.append(dataCollection.effiHistReader)
        if s in fitSequence:
            sequence.append(fitCollection.GetFitterObjects(self, s))
        if s is 'plotter':
            sequence.append(GetPlotterObject(self))
        if s is 'mixedToyStudier':
            sequence.append(mixedToyValidation.GetMixedToyObject(self))
        if s in ['bkgCombToyGenerator', 'bkgPeakToyGenerator']:
            import LifetimeFitter.toyCollection as toyCollection
            sequence.append(toyCollection.GetToyObject(self, s))
        if s is 'FinalDataResult': sequence.append(dataCollection.FinalDataResult)
        if s is 'EffiTable': sequence.append(dataCollection.EffiTable)
    return sequence

def SetBatchTaskWrapper(args):
    from LifetimeFitter.anaSetup import modulePath
    if args.seqKey=='sigMCValidation':
        import LifetimeFitter.script.batchTask_sigMCValidation as sigMCValidation
        wrappedTask = sigMCValidation.BatchTaskWrapper(
            "myBatchTask",
            os.path.join(modulePath, "batchTask_sigMCValidation"),
            cfg=batchTask_sigMCValidation.setupBatchTask)
    elif args.seqKey=='mixedToyValidation':
        wrappedTask = mixedToyValidation.BatchTaskWrapper(
            "myBatchTask_MixedToy",
            os.path.join(modulePath, "batchTask_mixedToyValidation"),
            cfg=mixedToyValidation.setupBatchTask)
    else:
        import LifetimeFitter.script.batchTask_seqCollection as batchTask_seqCollection
        wrappedTask = batchTask_seqCollection.BatchTaskWrapper(
            "BatchTaskseqCollection",
            os.path.join(modulePath, "batchTask_seqCollection"),
            cfg=batchTask_seqCollection.setupBatchTask )
    parser.set_defaults(wrapper=wrappedTask, process=p)

    args = parser.parse_args()
    if args.Function_name=='postproc':
        if args.seqKey=='sigMCValidation':
            args.func = sigMCValidation.func_postproc
        if args.seqKey=='mixedToyValidation':
            args.func = mixedToyValidation.func_postproc

    if args.OneStep is False: args.TwoStep = True
    return args
     
if __name__ == '__main__':
    from LifetimeFitter.StdProcess import p
    from LifetimeFitter.anaSetup import tbins
    from LifetimeFitter.python.datainput import GetInputFiles
    import LifetimeFitter.script.batchTask_mixedToyValidation as mixedToyValidation
    from copy import deepcopy
   
    if args.OneStep is False: args.TwoStep = True

    p.work_dir= args.plotsPath+"/plots_{}".format(args.Year)
    p.cfg['args'] = deepcopy(args)
    p.cfg['sysargs'] = sys.argv
    GetInputFiles(p)
    p.cfg['bins'] = p.cfg['allBins'] if args.binKey=="all" else [key for key in tbins.keys() if tbins[key]['label']==args.binKey]

    if (not args.SimFit) and (type(predefined_sequence[args.seqKey]) is tuple):
        predefined_sequence[args.seqKey]=predefined_sequence[args.seqKey][0]+predefined_sequence[args.seqKey][1]
    if args.SimFit or args.SimFitPlots: 
        p.name="SimultaneousFitProcess"
        p.work_dir="plots_simultaneous"
    if args.seqKey=='sigMCValidation': p.name='sigMCValidationProcess'

    if args.Function_name in ['submit', 'run', 'postproc']:
        args = SetBatchTaskWrapper(args)
        p.cfg['args'] = deepcopy(args)

    for b in p.cfg['bins']:
        Stime = datetime.datetime.now()
        p.cfg['binKey'] = b
        def runSimSequences():
            for Year in [2016, 2017, 2018]:
                p.cfg['args'].Year=Year
                GetInputFiles(p)
                sequence=Instantiate(p, predefined_sequence[args.seqKey][0])
                p.setSequence(sequence)
                p.beginSeq()
                p.runSeq()
            p.cfg['args'].Year=args.Year
        if args.SimFit and not (args.Function_name in ['submit', 'run', 'postproc']):
            print ("INFO: Processing simultaneously over three year data")
            runSimSequences()
            sequence=Instantiate(p, predefined_sequence[args.seqKey][1])
            p.setSequence(sequence)
            p.beginSeq()
        elif args.Function_name in ['submit', 'run', 'postproc']:
            print("INFO: Processing {0} year data".format(args.Year))
            if p.cfg['args'].SimFit:
                if not args.Function_name=='submit':runSimSequences()
                sequence=Instantiate(p, predefined_sequence[args.seqKey][1])
            else:
                sequence=Instantiate(p, predefined_sequence[args.seqKey])
            p.setSequence(sequence)
            args.func(args) 
            continue
        else:
            print("INFO: Processing {0} year data".format(args.Year))
            sequence=Instantiate(p, predefined_sequence[args.seqKey])
            p.setSequence(sequence)
            p.beginSeq()
        p.runSeq()
        p.endSeq()
        for obj in p._sequence: obj.reset()
        Etime = datetime.datetime.now()
        print("Time taken to execute", args.seqKey, "in", b, "bin:", (Etime-Stime).seconds/60, "minutes!")
